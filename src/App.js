import React from 'react'
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom'
// styles
import './styles/styles.scss'
// components
import { CreateMatchEnhanced } from './app/CreateMatch'
import { MatchSummaryEnhanced } from './app/MatchSummary'
import { ArenaEnhanced } from './app/Arena'

const App = () => {
  return (
      <Router>
        <Switch>
          <Route path="/match/create"
                 component={CreateMatchEnhanced} />
          <Route path="/match/:matchid/:roundid?"
                 component={MatchSummaryEnhanced} />
          <Route path="/arena/:matchid/:roundid"
                 component={ArenaEnhanced} />
          <Redirect from="/" to="/match/create"/>
        </Switch>
      </Router>
  )
}

export default App
