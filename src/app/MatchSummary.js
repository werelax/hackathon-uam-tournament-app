import { get } from 'lodash'
import { compose } from 'recompose'
import withServices from './lib/with-services'
import withProps from './lib/with-props'
import withEffect from './lib/with-effect'
import { MatchSummary } from '../ui/views/MatchSummary'

export const MatchSummaryEnhanced = compose(
  withServices,
  withProps(({ services, match: urlmatch }) => {
    const matchId = get(urlmatch, 'params.matchid', 1)
    const match = services.scoreService.getMatch(matchId)
    const lastRound = services.scoreService.getLastRound(matchId)
    const aggregatedScore = services.scoreService.aggregatedScore(matchId)
    const roundId = services.scoreService.getRoundId(matchId)
    const nextRoundId = services.scoreService.getNextRoundId(matchId)
    return {
      matchId,
      match,
      drivers: match.drivers,
      lastRound,
      aggregatedScore,
      roundId,
      nextRoundId
    }
  }),
)(MatchSummary)
