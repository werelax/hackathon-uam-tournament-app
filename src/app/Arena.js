import { get } from 'lodash'
import { compose, withProps } from 'recompose'
import withEffect from './lib/with-effect'
import withServices from './lib/with-services'
import { Arena } from '../ui/views/Arena'

export const ArenaEnhanced = compose(
  withServices,
  withProps(({ services, match: urlMatch }) => {
    const matchId = get(urlMatch, 'params.matchid', 1)
    const roundId = get(urlMatch, 'params.roundid', 1)
    const match = services.scoreService.getMatch(matchId)
    return {
      matchId,
      roundId,
      driverIds: match.drivers,
      pauseGame: () => services.matchService.pauseGame(),
      endGame: () => services.matchService.endGame(),
      worldState: services.matchService.getWorldState(),
      isGamePaused: services.matchService.isGamePaused(),
      isGameEnded: services.matchService.isGameEnded()
    }
  }),
  withEffect(({ matchId, roundId, services, driverIds }) => {
    services.matchService.startGame(matchId, roundId, driverIds)
    return () => services.matchService.destroyGame()
  }, props => [props.matchId, props.roundId]),
)(Arena)
