import React, { useEffect } from 'react'

const withEffect = (hook, getValues) => Component => (props) => {
  const values = getValues(props)
  useEffect(() => hook(props), values)
  return (<Component {...props}/>)
}

export default withEffect
