import { assign } from 'lodash'
import { useState } from 'react'
import matchService from '../../services/match-service'
import scoreService from '../../services/score-service'

export default (infra) => {
  const services = {}
  return assign(services, {
    matchService: matchService(infra, ...useState(), services),
    scoreService: scoreService(infra, ...useState(), services)
  })
}
