import { compose, withProps } from 'recompose'
import { CreateMatch } from '../ui/views/CreateMatch'
import withServices from './lib/with-services'

export const CreateMatchEnhanced = compose(
  withServices,
  withProps(({ services, history }) => ({
    previousMatches: services.scoreService.listAllMatches(),
    onSubmit: (matchId, driverIds) => {
      services.scoreService.createMatch(matchId, driverIds)
      history.push(`/match/${matchId}`)
    }
  }))
)(CreateMatch)
