import React from 'react'
import PropTypes from 'prop-types'
import Select from '../forms/Select'
import Robot from '../robots/Robot'

const RobotSelect = (props) => {
  const { title, name, id, options, bg, onChange } = props
  return (
    <div className='robot-select'>
      <Robot bg={bg} title={ title} />
      <Select name={name} id={id} options={options} onChange={onChange}/>
    </div>
  )
}

RobotSelect.propTypes = {
  title: PropTypes.string,
  bg: PropTypes.string,
  name: PropTypes.string,
  id: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.object)
}

export default RobotSelect
