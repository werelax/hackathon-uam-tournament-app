import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

const defaultOnChange = () =>
  console.error('Using default onChange implementation')

const Input = props => {
  const className = cx('form-field', props.className, {
    error: props.error,
    disabled: props.disabled
  })
  return (
    <div className={className}>
      {props.label &&
        <label htmlFor={props.id} className="form-label">
          {props.label}
        </label>
      }
      <div className="form-container">
        <input
          className={'form-input'}
          type={props.type}
          name={props.name}
          id={props.id}
          value={props.value}
          onSubmit={props.onBlur}
          onChange={props.onChange || defaultOnChange}
          onFocus={props.onFocus}
          placeholder={props.placeholder}
          onBlur={props.onBlur}
          disabled={props.disabled ? 'disabled' : ''}
          readOnly={props.readOnly}
        />
      </div>
      {props.helperText && <span className="helper-text">{props.helperText}</span>}
    </div>
  )
}

Input.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  id: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  name: PropTypes.string,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  iconName: PropTypes.string,
  error: PropTypes.string,
  disabled: PropTypes.bool,
  readOnly: PropTypes.bool,
  helperText: PropTypes.string
}

Input.defaultProps = {
  type: 'text'
}

export default Input
