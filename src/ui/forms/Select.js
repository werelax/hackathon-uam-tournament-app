import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

const Select = props => {
  const className = cx('select-field', {
    'has-error': props.error,
    'is-disabled': props.disabled
  })
  return (
    <div className={className}>
      { props.label && <label className="form-label" htmlFor={props.id}>{props.label}</label> }
      <div className="form-container">
        <select
          autoComplete={'off'}
          className={'form-select'}
          name={props.name}
          id={props.id}
          value={props.value}
          onSubmit={props.onBlur}
          onChange={props.onChange}
          onFocus={props.onFocus}
          onBlur={props.onBlur}
          disabled={props.disabled ? 'disabled' : ''}
        >
          {props.options.map(({ value, label, selected }, i) => (
            <option
              className="option"
              value={value}
              key={i}
              selected={selected}
            >
              {label}
            </option>
          ))}
        </select>
      </div>
      { props.helperText && <span className="helper-text">{props.helperText}</span> }
    </div>
  )
}

Select.propTypes = {
  id: PropTypes.string,
  value: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  helperText: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  option: PropTypes.arrayOf(PropTypes.object)
}

export default Select
