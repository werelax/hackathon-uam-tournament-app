import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

const Value = (props) => {
  const classNames = cx('value',{
    'strong': props.strong,
    'success': props.success,
    'info': props.info,
    'error': props.error
  })
  return (
    <span className={ classNames }>{ props.value }</span>
  )
}

Value.propTypes = {
  value: PropTypes.string,
  strong: PropTypes.bool,
  success: PropTypes.bool,
  info: PropTypes.bool,
  error: PropTypes.bool
}

export default Value
