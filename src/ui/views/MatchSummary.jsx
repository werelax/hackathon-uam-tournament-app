import { find } from 'lodash'
import React from 'react'
import { Link } from 'react-router-dom'
import Button from '../buttons/Button'
import Robot from '../robots/Robot'
import Value from '../texts/Value'

function col(drivers, id) {
  const { color } = find(drivers, { id })
  return color
}

const ScoreRow = ({ score: scoreObj, pos, drivers }) => {
  const { id, score, kills, killedBy, inflictedDamage } = scoreObj
  return (
    <tr>
      <th><Value strong value={pos} /></th>
      <th><Robot bg={col(drivers, id)} title={id} /></th>
      <th><Value success value={score} /></th>
      <th><Value value={kills.length} /></th>
      <th>{ killedBy && <Robot bg={col(drivers, killedBy)}
                               title={killedBy} />}</th>
      <th><Value error value={inflictedDamage} /></th>
    </tr>
  )
}

const ScoreTable = ({ score, drivers }) => {
  const { id, results } = score
  return (
    <div className='table-wrapper'>
      <table className='table'>
        <thead>
          <tr>
            <th>Posición</th>
            <th>Robot</th>
            <th>Puntos</th>
            <th>Víctimas</th>
            <th>Eliminado por</th>
            <th>Daño causado</th>
          </tr>
        </thead>
        <tbody>
          {results.map((row, i) => (
            <ScoreRow score={row} pos={i + 1} drivers={drivers} key={row.id}/>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export const MatchSummary = (props) => {
  const { roundId, matchId, match, drivers, lastRound, aggregatedScore, nextRoundId } = props
  return (
    <div className='full-content'>
      <div className='panel full-size'>
        <h1 className='alpha'>Combate {matchId}. Ronda {roundId}.</h1>
        <ScoreTable drivers={drivers} score={lastRound}/>

        <h1 className='alpha'>Combate {matchId}. Puntuación Acumulada.</h1>
        <ScoreTable drivers={drivers} score={aggregatedScore}/>

        <div className='panel-actions'>
          <Link className="button secondary" to='/match/create'>Volver</Link>
          <Link className="button" to={`/arena/${matchId}/${nextRoundId}`}>Combatir!</Link>
        </div>
      </div>
    </div>
  )
}
