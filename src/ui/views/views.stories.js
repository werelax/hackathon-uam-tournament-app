import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import NewCombat from './NewCombat'
import ScoreCombat from './ScoreCombat'
import Arena from './Arena'
import ConfirmationModal from '../modals/ConfirmationModal'
import { MemoryRouter } from 'react-router-dom'

storiesOf('Views', module)

.addDecorator(story => (
  <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
))

  .add('New Combat', () => (
    <NewCombat items= {[
      {text: 'Combate akandemorl'},
      {text: 'Combate jarl'},
      {text: 'Combate pinguino'}
    ]}/>
  ))

  .add('Score Combat', () => (
    <ScoreCombat />
  ))

  .add('Confirmation modal', () => (
    <Fragment>
      <ScoreCombat />
      <ConfirmationModal />
    </Fragment>
  ))

  .add('Arena', () => (
    <Arena items= {[
      {image: 'styles/tank.png',  name: 'Killer Shark', barBg: '#00C176', barWidth: '100', value: '200', tinyValue1: '123ms', tinyValue2: 'scan(10, 10)'},
      {image: 'styles/tank.png',  name: 'Sniper', barBg: '#D0021B', barWidth: '20', value: '200', tinyValue1: '123ms', tinyValue2: 'scan(20, 10)'},
      {image: 'styles/tank.png',  name: 'Bit bytes', barBg: '#FEAD26', barWidth: '50', value: '200', tinyValue1: '2675ms', tinyValue2: '4556'},
      {image: 'styles/tank.png',  name: 'grunt', barBg: '#D0021B', barWidth: '30', value: '200', tinyValue1: '623ms', tinyValue2: 'scan(10, 10)'},
      {image: 'styles/tank.png',  name: 'Rabbit', barBg: '#D0021B', barWidth: '30', value: '200', tinyValue1: '623ms', tinyValue2: 'scan(10, 10)'}
    ]} />
  ))