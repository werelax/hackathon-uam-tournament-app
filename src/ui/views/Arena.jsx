import { sortBy } from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import RobotItem from '../robots/RobotItem'
import Button from '../buttons/Button'
import arena from '../../styles/images/arena.png'

export const Arena = (props) => {
  const { roundId, startGame, pauseGame, endGame, isGamePaused, isGameEnded, matchId } = props
  const { worldState: { tanks = [] } } = props
  return (
    <div className='arena-wrapper'>
      <div className="arena">
        <img src={arena} alt="arena"/>
      </div>
      <div className="arena-info">
        <h1 className="alpha">Ronda <span className='value'>{roundId}</span></h1>
        <ul className="robots-list">
          {sortBy(tanks, 'id').map((tank, i) => (
            <RobotItem { ...tank} key={i} />
          ))}
        </ul>
        <div className="actions">
          { isGamePaused
            ? <Button onClick={pauseGame} text='Continuar' />
            : <Button onClick={pauseGame} text='Pausa' />
          }

          { isGameEnded
            ? <Link className="button" to={`/match/${matchId}`}>Volver</Link>
            : <Button onClick={endGame} text='Finalizar' />
          }

        </div>
      </div>
    </div>
  )
}

Arena.propTypes = {
  onClick: PropTypes.func,
  items: PropTypes.array
}
