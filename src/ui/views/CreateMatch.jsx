import { keys, values, compact } from 'lodash'
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import RobotSelect from '../forms/RobotSelect'
import Input from '../forms/Input'
import Button from '../buttons/Button'
import LinkRef from '../links/LinkRef'
import { Link } from 'react-router-dom'
import { wrappedDrivers } from '../../simulation'
import { config } from '../../simulation/config'

const options = keys(wrappedDrivers).map((driver) => ({
  value: driver,
  label: driver
}))
options.unshift({ value: null, label: '---' })

const tankColors = config.tank.colors

export const CreateMatch = (props) => {
  const { previousMatches = [], onSubmit } = props
  const [selectedBots, setSelectedBot] = useState({})
  const randId = Math.random().toString(36).slice(2,7).toUpperCase()
  const selectBot = i => (e) => setSelectedBot({
    ...selectedBots, [i]: { color: tankColors[i], id: e.target.value }
  })
  const [combatName, setCombatName] = useState(randId)
  const updateName = (e) => setCombatName(e.target.value)
  const submit = (name, selectedBots) => {
    const driverIds = compact(values(selectedBots))
    onSubmit(name, driverIds)
  }
  return (
    <div className='full-content wide-space'>
      <div className='combats'>
        <div className="create-combat">
          <h1 className='alpha'>nuevo combate</h1>
          <div className="g">
            <div className='gi one-half'>
              <Input label='Nombre' id='input1'
                     value={combatName}
                     onChange={updateName}/>
            </div>
          </div>
          <div className="g">
            <div className='gi one-half'>
              <RobotSelect
                bg={tankColors[0]}
                name='robot-1'
                id='select1'
                options={options}
                onChange={selectBot(0)}
              />
              <RobotSelect
                bg={tankColors[1]}
                name='robot-2'
                id='select2'
                options={options}
                onChange={selectBot(1)}
              />
              <RobotSelect
                bg={tankColors[2]}
                name='robot-3'
                id='select3'
                options={options}
                onChange={selectBot(2)}
              />
            </div>
            <div className='gi one-half gutter'>
              <RobotSelect
                bg={tankColors[3]}
                name='robot-4'
                id='select4'
                options={options}
                onChange={selectBot(3)}
              />
              <RobotSelect
                bg={tankColors[4]}
                name='robot-5'
                id='select5'
                options={options}
                onChange={selectBot(4)}
              />
              <RobotSelect
                bg={tankColors[5]}
                name='robot-6'
                id='select6'
                options={options}
                onChange={selectBot(5)}
              />
            </div>
          </div>
          <div className='panel-actions'>
            <Button text='Comenzar'
                    onClick={() => submit(combatName, selectedBots)} />
          </div>
        </div>
        <div className="past-combats">
          <h1 className='alpha'>Combates anteriores</h1>
          <ul className="past-combats-list">
            {previousMatches.map((matchId, i) => (
              <li className='past-combats-item' key={i}>
                <LinkRef href={`/match/${matchId}`}>{matchId}</LinkRef>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  )
}
