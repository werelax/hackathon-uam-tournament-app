import React from 'react'
import PropTypes from 'prop-types'
import Button from '../buttons/Button'
import Robot from '../robots/Robot'
import Value from '../texts/Value'

const NewCombat = (props) => {
  return (
    <div className='full-content'>
      <div className='panel full-size'>
        <h1 className='alpha'>combate a-02. Ronda 0</h1>
        <div className='table-wrapper'>
          <table className='table'>
            <thead>
              <tr>
                <th>Posición</th>
                <th>Robot</th>
                <th>Puntos</th>
                <th>Víctimas</th>
                <th>Eliminado por</th>
                <th>Daño causado</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th><Value strong value='1' /></th>
                <th><Robot bg='#4A90E2' title='killer-shark' /></th>
                <th><Value success value='18' /></th>
                <th><Value value='13' /></th>
                <th><Robot bg='#359768' title='Bit bytes' /></th>
                <th><Value error value='3787' /></th>
              </tr>
              <tr>
                <th><Value strong value='2' /></th>
                <th><Robot bg='#BD10E0' title='grunt' /></th>
                <th><Value success value='23' /></th>
                <th><Value value='18' /></th>
                <th><Robot bg='#BD10E0' title='killer-shark' /></th>
                <th><Value error value='934' /></th>
              </tr>
              <tr>
                <th><Value strong value='3' /></th>
                <th><Robot bg='#F8E71C' title='saussage' /></th>
                <th><Value success value='16' /></th>
                <th><Value value='5' /></th>
                <th></th>
                <th><Value error value='0' /></th>
              </tr>
              <tr>
                <th><Value strong value='4' /></th>
                <th><Robot bg='#26EFFE' title='Rabbit' /></th>
                <th><Value success value='6' /></th>
                <th><Value value='2' /></th>
                <th><Robot bg='#BD10E0' title='grunt' /></th>
                <th><Value error value='287' /></th>
              </tr>
            </tbody>
          </table>
          <h1 className='alpha'>combate a-02. Ronda 0</h1>
          <table className='table'>
            <thead>
              <tr>
                <th>Posición</th>
                <th>Robot</th>
                <th>Puntos</th>
                <th>Víctimas</th>
                <th>Eliminado por</th>
                <th>Daño causado</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th><Value strong value='1' /></th>
                <th><Robot bg='#4A90E2' title='killer-shark' /></th>
                <th><Value success value='18' /></th>
                <th><Value value='13' /></th>
                <th><Robot bg='#359768' title='Bit bytes' /></th>
                <th><Value error value='3787' /></th>
              </tr>
              <tr>
                <th><Value strong value='2' /></th>
                <th><Robot bg='#BD10E0' title='grunt' /></th>
                <th><Value success value='23' /></th>
                <th><Value value='18' /></th>
                <th><Robot bg='#BD10E0' title='killer-shark' /></th>
                <th><Value error value='934' /></th>
              </tr>
              <tr>
                <th><Value strong value='3' /></th>
                <th><Robot bg='#F8E71C' title='saussage' /></th>
                <th><Value success value='16' /></th>
                <th><Value value='5' /></th>
                <th></th>
                <th><Value error value='0' /></th>
              </tr>
              <tr>
                <th><Value strong value='4' /></th>
                <th><Robot bg='#26EFFE' title='Rabbit' /></th>
                <th><Value success value='6' /></th>
                <th><Value value='2' /></th>
                <th><Robot bg='#BD10E0' title='grunt' /></th>
                <th><Value error value='287' /></th>
              </tr>
            </tbody>
          </table>
        </div>

        <div className='panel-actions'>
          <Button secondary text='finalizar' />
          <Button text='!Combatir!' />
        </div>
      </div>
    </div>
  )
}

NewCombat.propTypes = {
  onClick: PropTypes.func
}

export default NewCombat