import React from 'react'
import PropTypes from 'prop-types'

const OverlayWithModal = (props) => {
  return (
    <div className='overlay'>
      <div className='modal'>
        <button className='close-button'>X</button>
        { props.children }
      </div>
    </div>
  )
}

OverlayWithModal.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
}

export default OverlayWithModal
