import React from 'react'
import Input from '../forms/Input'
import OverlayWithModal from '../modals/OverlayWithModal'
import Button from '../buttons/Button'

const ConfirmationModal = (props) => {
  return (
    <OverlayWithModal>
      <h2 className='beta'>¿Seguro que quieres finalizar el combate?</h2>
      <p className='intro-text'>Se borrarán todos los resultados. Para confirmarlo, escribe el nombre del combate en el siguiente input:</p>
      <Input label='Nombre del combate' id='input1'/>
      <div className='modal-actions'>
        <Button text='Cancelar' />
        <Button secondary text='Aceptar' />
      </div>
    </OverlayWithModal>
  )
}


export default ConfirmationModal
