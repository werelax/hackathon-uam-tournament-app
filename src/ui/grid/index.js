import React from 'react'
import PropTypes from 'prop-types'

export const GridRow = (props) => (
  <div className='g'>
    { props.children }
  </div>
)

export const GridCol = (props) => (
  <div className={`gi ${props.type}`}>
    { props.children }
  </div>
)

GridCol.propTypes = {
  type: PropTypes.string
}

GridCol.defaultProps = {
  type: 'one-whole'
}
