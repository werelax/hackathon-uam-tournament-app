import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

const LinkRef = props => {
  return (
    <Link to={props.href || '#'} className='link'>
      <span className="text">{ props.children }</span>
    </Link>
  )
}

LinkRef.propTypes = {
  href: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
}

export default LinkRef
