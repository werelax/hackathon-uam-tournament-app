import React from 'react'
import Value from '../texts/Value'
import tank1 from '../../styles/images/tanks/tank-green.png'
import tank2 from '../../styles/images/tanks/tank-yellow.png'
import tank3 from '../../styles/images/tanks/tank-purple.png'
import tank4 from '../../styles/images/tanks/tank-blue.png'
import tank5 from '../../styles/images/tanks/tank-indigo.png'
import tank6 from '../../styles/images/tanks/tank-orange.png'
import cannon1 from '../../styles/images/tanks/canon-green.png'
import cannon2 from '../../styles/images/tanks/canon-yellow.png'
import cannon3 from '../../styles/images/tanks/canon-purple.png'
import cannon4 from '../../styles/images/tanks/canon-blue.png'
import cannon5 from '../../styles/images/tanks/canon-indigo.png'
import cannon6 from '../../styles/images/tanks/canon-orange.png'
import skull1 from '../../styles/images/tanks/skull-green.png'
import skull2 from '../../styles/images/tanks/skull-yellow.png'
import skull3 from '../../styles/images/tanks/skull-purple.png'
import skull4 from '../../styles/images/tanks/skull-blue.png'
import skull5 from '../../styles/images/tanks/skull-indigo.png'
import skull6 from '../../styles/images/tanks/skull-orange.png'

const lifebarColors = [
  { threshold: 30, color: '#ffff00' },
  { threshold: 60, color: '#ffff00' },
  { threshold: 100, color: '#00ff00' }
]

const tanks = [
  { skull: skull1, body: tank1, cannon: cannon1 },
  { skull: skull2, body: tank2, cannon: cannon2 },
  { skull: skull3, body: tank3, cannon: cannon3 },
  { skull: skull4, body: tank4, cannon: cannon4 },
  { skull: skull5, body: tank5, cannon: cannon5 },
  { skull: skull6, body: tank6, cannon: cannon6 }
]


const RobotItem = (props) => {
  const { lifePoints, id, rotation, position, dead, lag, lastCommand, cannon, color, colorIdx } = props
  const { body, cannon: cannonImage, skull } = tanks[colorIdx]
  const { color: lifeColor } = lifebarColors.find(({ threshold }) => {
    return lifePoints <= threshold
  })
  return (
    <li className='robot-item'>
      <div className='robot-image'>
        { dead && <img src={skull} alt={id} style={{
          width: '60px',
          position: 'absolute',
          top: '31px',
          left: '67px'
        }}/> }
        { !dead && <img src={body} alt={id} style={{
          position: 'absolute',
          top: '50px',
          left: '45px',
          transform: `rotate(${rotation}deg)`
        }}/> }
        { !dead && <img src={cannonImage} alt={id} style={{
          position: 'absolute',
          top: '52px',
          left: '75px',
          width: '80px',
          transformOrigin: '19px 50%',
          transform: `rotate(${cannon}deg)`
        }}/> }
      </div>
      <div className="robot-info">
        <p className="name">{ id }</p>
        <div className='progress-bar'>
          <span className="progress" style={{
            backgroundColor: lifeColor,
            width: `${ Math.max(0, lifePoints) }%`
          }}></span>
          <span className="value" style={{ color: lifeColor }}>
            { lifePoints }
          </span>
        </div>
        <div className="complementary-info">
          <Value tiny error value={ String(lag) } />
          <Value tiny info value={ lastCommand } />
        </div>
      </div>
    </li>
  )
}

export default RobotItem
