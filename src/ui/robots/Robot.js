import React from 'react'
import PropTypes from 'prop-types'

const Robot = (props) => {
  const { title,  bg } = props
  return (
    <div className='robot'>
      <span className='sample-color' style={{ backgroundColor: bg }}></span>
      <p className='robot-name'>{title}</p>
    </div>
  )
}

Robot.propTypes = {
  title: PropTypes.string,
  bg: PropTypes.string
}

export default Robot
