import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

const Button = (props) => {
  const classNames = cx('button',{
    'secondary': props.secondary
  })
  return (
    <button className={ classNames } onClick={props.onClick}>
      <span className='text'>{props.text}</span>
      <span className='reader-only'>{props.text}</span>
    </button>
  )
}

Button.propTypes = {
  onClick: PropTypes.func,
  secondary: PropTypes.bool,
  text: PropTypes.string
}

Button.defaultProps = {
  onClick: function () { }
}

export default Button