import * as simulation from '../simulation'

const initState = {
  matchId: null,
  roundId: null,
  game: null,
  worldState: {},
  isGamePaused: false,
  isGameEnded: false,
}

let service

export default ({ storage }, state = initState, setState, services) => {

  service = {
    // queries
    getWorldState: () => state.worldState,
    isGamePaused: () => state.isGamePaused,
    isGameEnded: () => state.isGameEnded,

    // commands

    startGame(matchId, roundId, driverIds) {
      const game = simulation.startGame(driverIds)
      window.game = game
      game.events.on('update-world-state',
                     (...args) => service.updateWorldState(...args))
      game.events.on('end-game',
                     (...args) => service.onGameEnded(...args))
      game.events.once('game-ready', () => setState({
        ...state, matchId, roundId, game
      }))
    },

    updateWorldState(worldState) {
      setState({ ...state, worldState })
    },

    onGameEnded(score) {
      const { scoreService } = services
      const { matchId, roundId } = state
      scoreService.saveScoreResults(matchId, roundId, score)
      setState({ ...state, isGameEnded: true })
    },

    pauseGame() {
      simulation.pauseGame(state.game, state.isGamePaused)
      setState({ ...state, isGamePaused: !state.isGamePaused})
    },

    endGame() {
      simulation.endGame(state.game)
    },

    destroyGame() {
      service._destroyGame()
    },

    _destroyGame() {
      simulation.destroyGame(state.game)
      service.reset()
    },

    reset() {
      setState(initState)
    }
  }

  return service
}
