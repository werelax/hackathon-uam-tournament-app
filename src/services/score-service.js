import { isEmpty, sortBy, find, values, reject, set, update, keys, last } from 'lodash'

const initState = { id: null, items: [] }

let service

export default ({ storage }, state = initState, setState) => {

  service = {
    // queries

    matchExists(matchId) {
      const matches = storage.get('matches', {})
      return matchId in matches
    },

    getMatch(matchId) {
      const matches = storage.get('matches', {})
      return matches[matchId] || { id: matchId, scores: [] }
    },

    getLastRound(matchId) {
      const match = service.getMatch(matchId)
      if (isEmpty(match.scores))
        return service.emptyTable(match.drivers)
      else
        return last(sortBy(values(match.scores), 'id'))
    },

    aggregatedScore(matchId) {
      const match = service.getMatch(matchId)
      if (isEmpty(match.scores))
        return service.emptyTable(match.drivers)
      const scores = sortBy(values(match.scores), 'id')
      const results = match.drivers.map(
        driver => service.aggregateDriverScore(driver, scores)
      )
      return {
        id: 'aggregated',
        results: sortBy(results, 'score').reverse()
      }
    },

    aggregateDriverScore(driver, scores) {
      const { id } = driver
      const driverScores = scores.map((roundScores) => {
        return find(roundScores.results, { id })
      })
      return driverScores.reduce((acc, el) => ({
        id: acc.id,
        score: acc.score + el.score,
        kills: acc.kills + el.kills,
        killedBy: el.killedBy,
        inflictedDamage: acc.inflictedDamage + el.inflictedDamage
      }))
    },

    getRoundId(matchId) {
      const match = service.getMatch(matchId)
      return last(sortBy(keys(match.scores))) || 0
    },

    getNextRoundId(matchId) {
      return Number(service.getRoundId(matchId)) + 1
    },

    emptyTable(drivers = []) {
      return {
        id: 0,
        results: drivers.map(({ id, color }) => ({
          id: id,
          score: 0,
          kills: 0,
          killedBy: '',
          inflictedDamage: 0
        }))
      }
    },

    listAllMatches() {
      return keys(storage.get('matches', {}))
    },

    // commands
    updateMatches(key, data) {
      storage.update('matches', (m = {}) => set(m, key, data))
    },

    updateMatch(matchId, fn) {
      storage.update(
        'matches', (matches = {}) => update(matches, matchId, fn)
      )
    },

    createMatch(matchId, driverIds) {
      if (service.matchExists(matchId)) return
      const match = { id: matchId, drivers: driverIds, scores: {} }
      service.updateMatches(matchId, match)
    },

    saveScoreResults(matchId, roundId, scoreResults) {
      const data = { id: roundId, results: values(scoreResults) }
      service.updateMatch(
        matchId, (m = {}) => update(
          m, 'scores', (s = {}) => set(s, roundId, data)
        )
      )
    }
  }

  return service
}
