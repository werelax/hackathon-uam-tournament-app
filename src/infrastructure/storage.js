export default () => {
  const storage = {
    get(key, defaultValue = {}) {
      const json = localStorage.getItem(key)
      return json ? JSON.parse(json) : defaultValue
    },
    set(key, value) {
      const jsonString = JSON.stringify(value)
      localStorage.setItem(key, jsonString)
    },
    update(key, updater) {
      const data = storage.get(key)
      storage.set(key, updater(data))
    },
    remove(key) {
      localStorage.removeItem(key)
    }
  }

  return storage
}
