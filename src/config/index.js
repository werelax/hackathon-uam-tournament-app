const configs = {
  develop: {
  }
}

const env = process.env.NODE_ENV
const activeConfig = (configs[env] || configs.develop)
export default activeConfig
