async function main(tank) {

var flag = 0;
var x = await tank.getX();
var y = await tank.getY();
var distN = 1000 - y;
var distE = 1340 - x;
var distW = x;
var distS = y;
var obj;
var r;
var r1,r2;
while(true){
  
  if (flag == 0){

	//var min = math.min(distS,distW,distE,distN);
	//var min = 5;
	//await tank.drive(90,50);
	var min = 1500;
	if(min > distN){
		min = distN;
	}
	if(min > distS){
		min = distS;
	}
	if(min > distE){
		min = distE;
	}
	if(min > distW){
		min = distW;
	}
	console.log(distN);
	console.log(distS);
	console.log(distE);
	console.log(distW);
	console.log(min);

	if(min == distN){
		while( await tank.getY() < 900){
			obj = await tank.scan(90,10);
			while(obj != 0){
				//disparar
				obj = await tank.scan(90,10);
			}
			await tank.drive(90,50);
			await tank.shoot(0,50);
		}
	}else if(min == distS){
		while( await tank.getY() > 100 ){
			obj = await tank.scan(270,10);
			while(obj != 0){
				//disparar
				obj = await tank.scan(270,10);
			}
			await tank.drive(270,50);
		}

	}else if(min == distE){
		while( await tank.getX() < 1200){
			obj = await tank.scan(0,10);
			while(obj != 0){
				//disparar
				obj = await tank.scan(0,10);
			}
			await tank.drive(0,50);
		}
	}else{
		while( await tank.getX() > 100){
			obj = await tank.scan(180,10);
			while(obj != 0){
				//disparar
				obj = await tank.scan(180,10);
			}
			await tank.drive(180,50);
		}
	}
	flag = 1;
  }
  
 	await tank.shoot(50,50);
  	while(await tank.getX() < 1200 && await tank.getY() > 900){ //pared arriba
  		r = await tank.scan(0,10);
  		while(r != 0){
  			if(r < 500){
  				await tank.shoot(5,r+20);
  				await tank.shoot(345,r+20);
  			}
  			r = await tank.scan(0,10);
  		}
  		r1 = await tank.scan(240,10);
  		r2 = await tank.scan(290,10);
  		while(r1 != 0 || r2 != 0){
	  		if(r1!=0){
	  			await tank.shoot(235,r1+20);
	  			await tank.shoot(245,r1+20);
	  			await tank.drive(180,50);
	  		}
	  		if(r2!=0){
	  			await tank.shoot(295,r2+20);
	  			await tank.shoot(285,r2+20);
	  			await tank.drive(180,50);
	  		}
	  	r1 = await tank.scan(240,10);
  		r2 = await tank.scan(290,10);
  		}
  		await tank.drive(0,50);
  	}
  	while(await tank.getY() > 100){//pared derecha
  		r = await tank.scan(270,10);
  		while(r != 0){
  			if(r < 500){
  				await tank.shoot(260,r+20);
  				await tank.shoot(280,r+20);

  			}
  		}
  		r1 = await tank.scan(170,10);
  		r2 = await tank.scan(190,10);
  		while(r1 != 0 || r2 != 0){
	  		if(r1!=0){
	  			await tank.shoot(165,r1+20);
	  			await tank.shoot(175,r1+20);
	  			await tank.drive(270,50);
	  		}
	  		if(r2!=0){
	  			await tank.shoot(195,r2+20);
	  			await tank.shoot(185,r2+20);
	  			await tank.drive(270,50);
	  		}
	  	r1 = await tank.scan(170,10);
  		r2 = await tank.scan(190,10);
  		}
  		await tank.drive(270,50);
  	}
  	while(await tank.getX() > 150){ //pared abajo
  		r = await tank.scan(180,10);
  		while(r != 0){
  			if(r < 500){
  				await tank.shoot(190,r+20);
  				await tank.shoot(170,r+20);

  			}
  		}
  		r1 = await tank.scan(70,10);
  		r2 = await tank.scan(110,10);
  		while(r1 != 0 || r2 != 0){
	  		if(r1!=0){
	  			await tank.shoot(65,r1+20);
	  			await tank.shoot(75,r1+20);
	  			await tank.drive(90,50);
	  		}
	  		if(r2!=0){
	  			await tank.shoot(115,r2+20);
	  			await tank.shoot(105,r2+20);
	  			await tank.drive(90,50);
	  		}
	  	r1 = await tank.scan(70,10);
  		r2 = await tank.scan(110,10);
  		}
  		await tank.drive(180,50);
  	}
  	while(await tank.getY() < 900 ){//pared izquierda
  		r = await tank.scan(90,10);
  		while(r != 0){
  			if(r < 500){
  				await tank.shoot(95,r+20);
  				await tank.shoot(85,r+20);
  			}
  		}
  		r1 = await tank.scan(10,10);
  		r2 = await tank.scan(350,10);
  		while(r1 != 0 || r2 != 0){
	  		if(r1!=0){
	  			await tank.shoot(5,r1+20);
	  			await tank.shoot(15,r1+20);
	  			await tank.drive(0,50);
	  		}
	  		if(r2!=0){
	  			await tank.shoot(355,r2+20);
	  			await tank.shoot(345,r2+20);
	  			await tank.drive(0,50);
	  		}
	  	r1 = await tank.scan(10,10);
  		r2 = await tank.scan(350,10);
  		}
  		await tank.drive(90,50);
  	}
  	//await tank.drive(0,0);
  }
  return;
}


// funcion de acercamiento a una pared
async function acercarse_pared(tank){
	var x = await tank.getX();
	var y = await tank.getY();
	var distN = 1000 - y;
	var distE = 1340 - x;
	var distW = x;
	var distS = y;
	var obj;
	//var min = math.min(distS,distW,distE,distN);
	//var min = 5;
	//await tank.drive(90,50);
	var min = 1500;
	if(min > distN){
		min = distN;
	}
	if(min > distS){
		min = distS;
	}
	if(min > distE){
		min = distE;
	}
	if(min > distW){
		min = distW;
	}
	console.log(distN);
	console.log(distS);
	console.log(distE);
	console.log(distW);
	console.log(min);

	if(min == distN){
		while( await tank.getY() < 900){
			obj = await tank.scan(90,10);
			while(obj != 0){
				//disparar
				obj = await tank.scan(90,10);
			}
			await tank.drive(90,50);
			await tank.shoot(0,50);
		}
	}else if(min == distS){
		while( await tank.getY() > 100 ){
			obj = await tank.scan(270,10);
			while(obj != 0){
				//disparar
				obj = await tank.scan(270,10);
			}
			await tank.drive(270,50);
		}

	}else if(min == distE){
		while( await tank.getX() < 1200){
			obj = await tank.scan(0,10);
			while(obj != 0){
				//disparar
				obj = await tank.scan(0,10);
			}
			await tank.drive(0,50);
		}
	}else{
		while( await tank.getX() > 100){
			obj = await tank.scan(180,10);
			while(obj != 0){
				//disparar
				obj = await tank.scan(180,10);
			}
			await tank.drive(180,50);
		}
	}
	await tank.drive(270,50);
	//return;

}
