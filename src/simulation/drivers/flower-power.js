async function main(tank) {
  var disparo1 = 0;
  var disparo2 = 0;
  // Estrategia inicial, disparar a las 4 esquinas
  var cont = 0;
  if(await tank.scan(45, 10) != 0){
    await tank.shoot(45, 700);
    cont++;
    disparo1 = 45;
  }
  if(await tank.scan(135, 10) != 0){
    await tank.shoot(135, 700);
    if(cont == 1) {
      disparo2 = 135;
    } else {
      disparo1 = 135;
    }
    cont++;
  }
  if(cont < 2){
    if(await tank.scan(225, 10) != 0) {
      await tank.shoot(225, 700);
      if(cont == 1) {
        disparo2 = 225;
      } else {
        disparo1 = 225;
      }
      cont++;
    }
  }
  if(cont < 2){
    if(await tank.scan(315, 10) != 0){
      await tank.shoot(315, 700);
      disparo2 = 315;
      cont++;
    }
  }

  // Retirada inicial
  var arriba = false;
  var abajo = false;
  var derecha = false;
  var izquierda = false;
  if(cont == 2) {
    // Abajo
    if( (disparo1 == 45 && disparo2 == 135) || (disparo2 == 45 && disparo1 == 135) ) {
      await tank.drive(350, 100);
      abajo = true;
      while( await tank.getY() > 100) null;
      await tank.drive(0, 0);

    // Derecha
    } else if( (disparo1 == 135 && disparo2 == 225) || (disparo2 == 135 && disparo1 == 225) ) {
      await tank.drive(65, 100);
      derecha = true;
      while( await tank.getY() < 700) null;
      await tank.drive(0, 0);

    // Arriba
    } else if( (disparo1 == 225 && disparo2 == 315) || (disparo2 == 225 && disparo1 == 315) ) {
      await tank.drive(165, 100);
      arriba = true;
      while( await tank.getY() < 900) null;
      await tank.drive(0, 0);

    // Izquierda
    } else if( (disparo1 == 315 && disparo2 == 45) || (disparo2 == 315 && disparo1 == 45) ) {
      await tank.drive(260, 100);
      izquierda = true;
      while( await tank.getY() > 300) null;
      await tank.drive(0, 0);

    }
  }

  // Angulo inicial de escaneo
  var angulo = 0;
  // Arriba
  if( arriba == true ) {
    angulo = 230;
  // Abajo
  } else if( abajo == true ) {
    angulo = 50;
  // Derecha
  } else if( derecha == true ) {
    angulo = 150;
  // Izquierda
  } else if( izquierda == true ) {
    angulo = 345;
  }

  // Bucle principal
  var direccion = 0;
  var distancia = 0;
  var coorX = -1;
  var coorY = -1;
  var cont = 0;
  while(true) {
    distancia = 0;
    // Deteccion colisiones
    coorX = await tank.getX();
    coorY = await tank.getY();
      // Vamos a chocarnos con la de abajo
    if( coorY < 200 ) {
      await tank.drive(0, 0);
      if( coorX <= 670) {
        await tank.drive(0, 80);
      } else {
        await tank.drive(180, 80);
      }
    }
      // Vamos a chocarnos con la de la izquierda
    if( coorX < 200 ) {
      await tank.drive(0, 0);
      if( coorY <= 500) {
        await tank.drive(90, 80);
      } else {
        await tank.drive(270, 80);
      }
    }
      // Vamos a chocarnos con la de arriba
    if( coorY > 800 ) {
      await tank.drive(0, 0);
      if( coorX <= 670) {
        await tank.drive(0, 80);
      } else {
        await tank.drive(180, 80);
      }
    }
      // Vamos a chocarnos con la de la derecha
    if( coorX > 1140 ) {
      await tank.drive(0, 0);
      if( coorY <= 500) {
        await tank.drive(90, 80);
      } else {
        await tank.drive(270, 80);
      }
    }

    // ESCANEO
    distancia = await tank.scan(angulo, 10);
    if(distancia < 200 && distancia != 0) {
      await tank.drive(direccion-90, 70);
      direccion += 75;
    }
    if( distancia != 0 && distancia > 500) { // SOLO HE CAMBIADO ESTE
      await tank.drive(angulo-5, 60);
      await tank.shoot(angulo, 700);
      await tank.shoot(angulo, 300);
      direccion = angulo-5;
      // Busqueda cercana
        // Poquito antes
      if (await tank.scan(angulo-6, 6) != 0) {
        await tank.drive(angulo-20, 60);
        await tank.shoot(angulo-6, 700);
        await tank.shoot(angulo-6, 300);
        direccion = angulo-20;
        angulo -= 6;
      }
        // En el mismo sitio
      if (await tank.scan(angulo, 6) != 0) {
        await tank.drive(angulo-10, 60);
        await tank.shoot(angulo, 700);
        await tank.shoot(angulo, 300);
        direccion = angulo-10;
      }
        // Poquito despues
      if (await tank.scan(angulo+6, 6) != 0) {
        await tank.drive(angulo, 60);
        await tank.shoot(angulo+6, 700);
        await tank.shoot(angulo+6, 300);
        direccion = angulo;
        angulo += 6;
      }
      angulo -= 20;
    } else {
      angulo += 20;
    }
    if (cont == 0) {
      await tank.shoot(0, 100);
      cont++;
    } else if(cont == 1){
      await tank.shoot(90, 100);
      cont++;
    } else if(cont == 2) {
      await tank.shoot(180, 100);
      cont++;
    } else {
      await tank.shoot(270, 100);
      cont = 0;
    }


  }
}
