
var maxX = 1310;
var maxY = 1000;

var cx = maxX / 2;

var cy = maxY / 2;

var posX = 0;
var posY = 0;

var realSpeed = 0;
var speed = 0;
var damage = 0;

var angulo = 0;
var cuadrante = 0;
var angulo_ultimo = -1;
var damage_anterior = 0;
var direccion = 0;

/*
if (posX >= cx && posY > cy) {
  await tank.shoot(180, 800);
  await tank.shoot(230, 800);

} else if (posX > cx && posY < cy) {
  await tank.shoot(180, 800);
  await tank.shoot(120, 800);

} else if (posX < cx && posY > cy) {
  await tank.shoot(0, 800);
  await tank.shoot(300, 800);

} else {
  await tank.shoot(0, 800);
  await tank.shoot(50, 800);
 */

async function mover(tank, a, v) {
  await tank.drive(a,v)
  //angulo = a;
  direccion = a;
}

 async function cuadrante_f() {
   if (posX >= cx && posY >= cy) {
     return 1;

   } else if (posX >= cx && posY <= cy) {
     return 2;

   } else if (posX <= cx && posY >= cy) {
     return 3;

   } else {
     return 4;
   }
 }


async function disparar(tank, angulo_disparo, distancia) {

  await tank.shoot(angulo_disparo, distancia);

  //await mover(tank,(360 + angulo_disparo -70 +  Math.ceil(140*Math.random()))%360,50);

}

async function estado (tank){
  posX = await tank.getX();
  posY = await tank.getY();
  realSpeed = await tank.getSpeed();
  damage_anterior = damage;
  damage = await tank.getDamage();

  if (damage < damage_anterior) {
    mover(tank, (direccion -90 + Math.ceil(180*Math.random()))%360,50);
  }

  cuadrante = await cuadrante_f()

}

async function angulo_centro() {


  var angulo = Math.acos((cx - posX) / Math.sqrt((cx-posX)**2 + (cy-posY)**2));
  angulo *= (360.0 / (2.*Math.pi));

  return angulo;

}

async function danio(tank){
  if (damage < damage_anterior){
    await mover(tank, Math.ceil(Math.random()*360), 100);
  }
}

async function iniciar(tank) {

  await estado(tank);



  if (posX >= cx && posY >= cy) {
    await tank.shoot(180, 800);
    await tank.shoot(230, 800);

  } else if (posX >= cx && posY <= cy) {
    await tank.shoot(180, 800);
    await tank.shoot(120, 800);

  } else if (posX <= cx && posY >= cy) {
    await tank.shoot(0, 800);
    await tank.shoot(300, 800);

  } else {
    await tank.shoot(0, 800);
    await tank.shoot(50, 800);
  }


  await mover(tank, Math.ceil(Math.random()*360), 50);


}





async function alejar_bordes(tank) {
  if ((maxX - posX) < 200 ) {

    await mover(tank, (180 - 30 + Math.ceil(60*(Math.random())))%360, 50);
  } else if ((posX) < 200 ) {
    await mover(tank,  - 30 + Math.ceil(60*(Math.random()))%360, 50);

  } else if ((posY) < 200) {
    await mover(tank, (90  - 30 + Math.ceil(60*(Math.random())))%360, 50);
    angulo = 90;

  } else if ((maxY - posY) < 200) {
    await mover(tank, (270  - 30 + Math.ceil(60*(Math.random())))%360, 50);
  }

}

async function escaneo_follow(tank, angulo, distancia, barrido, nums) {

  if (nums == 0) return;

  for(i=-1; i<2; i++) {

    var a = await tank.scan((angulo + barrido * i)%360, 5);

    if (a != 0) {
      angulo_ultimo = (angulo + barrido * i)%360;
      await disparar(tank, angulo_ultimo, Math.min(700, a+150));
      await escaneo_follow(tank, angulo_ultimo, a, Math.floor(barrido/2), nums-1);
      return;
    }

  }
  var ang = angulo_ultimo;
  angulo_ultimo = -1;
  await escaneo_follow(tank, ang, 0, Math.floor(1.5*barrido)%360, nums-1);

}

async function escanear(tank) {

  var actual = 0;
  var num = 10;
  for(var i=0; i < num; i++) {

    var a = await tank.scan((angulo + (360/num) * i)%360, 10);
    if (a != 0) {
      await disparar(tank, (angulo + (360/num) * i)%360, Math.min(700, a+150));
      await escaneo_follow(tank, (angulo + (360/num) * i)%360, a, 30, 3);
      return;
    }
  }





}

async function escanear_total(tank) {

  if(angulo_ultimo == -1){
    await escanear(tank);
  } else {
    await escaneo_follow(tank, angulo_ultimo, 0, 30, 2);
  }
}

async function main(tank) {


  await iniciar(tank);

  while(true) {
    await estado(tank);
    await alejar_bordes(tank);
    await escanear_total(tank);

  }


}
