async function aimShoot(tank, cuadrante) {
	var angle, resolution = 10, dist;
	var i;
	var init, fin, inc = 18

	if(cuadrante == 1) {
		init = 250;
		fin = 380;
	}

	else if(cuadrante == 2) {
		init = 160;
		fin = 290;
	}

	else if(cuadrante == 3) {
		init = -20;
		fin = 120;
	}

	else if(cuadrante == 4) {
		init = 70;
		fin = 200;
	}

	else{
		if(Math.floor((Math.random() * 2)) < 1) init = 0;
		else init = 45;

		fin = 359;
		inc = 90;
		//resolution = 7;
	}

	for(i = init; i < fin; i += inc) {
		var x, y;
		x = await tank.getX();
		y = await tank.getY();
		if(x < 230 || x > 1110 || y < 230 || y > 770)
			break;

		if((dist = await tank.scan(i, resolution)) != 0) {
			if (dist < 50) dist = 150;
			await tank.shoot(i + 1, dist);
			await tank.shoot(i - 1, dist);
			break;
		}
	}


}

async function move(tank, cuadrante) {
	var angle, speed = 65;
	var randAngle = 90;

	// if(await tank.getDamage() > 75)
		// randAngle = 12;

	// Cuadrante central
	if(cuadrante == 0) {
		angle = Math.floor((Math.random() * 359));
		speed = 100;
	}

	// Cuadrante 1
	else if(cuadrante == 1) {
		angle = Math.floor((Math.random() * randAngle) + 270);
	}

	// Cuadrante 2
	else if(cuadrante == 2) {
		angle = Math.floor((Math.random() * randAngle) + 180);
	}

	// Cuadrante 3
	else if(cuadrante == 3) {
		angle = Math.floor((Math.random() * randAngle));
	}

	// Cuadrante 4
	else {
		angle = Math.floor((Math.random() * randAngle) + 90);
	}

	// Movimiento
	await tank.drive(angle, speed);
}

async function main(tank) {
	var x, y;
	var cuadrante;
	while(true) {
		x = await tank.getX();
		y = await tank.getY();
		// Cuadrante central
		if(y >= 333 && y <= 666 && x >= 447 && x <= 893) {
			cuadrante = 0;
		}

		// Cuadrante 1
		else if(y > 500 && x <= 670) {
			cuadrante = 1;
		}

		// Cuadrante 2
		else if(y >= 500 && x > 670) {
			cuadrante = 2;
		}

		// Cuadrante 3
		else if(y <= 500 && x < 670) {
			cuadrante = 3;
		}

		// Cuadrante 4
		else if(y < 500 && x >= 670) {
			cuadrante = 4;
		}

		else {
			cuadrante = 0;
		}

		await move(tank, cuadrante);

		await aimShoot(tank, cuadrante);
	}
}
