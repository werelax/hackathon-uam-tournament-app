async function superScan (tank) {
  var sc = []
  sc['angle'] = 0
  sc['resolution'] = 10
  var d
  while (true) {
    if (await tank.getDamage() > 70) {
      await defensaAbsoluta(tank)
    }
    d = await tank.scan(sc['angle'], sc['resolution'])
    if (d === 0) {
      sc['angle'] = (sc['angle'] + 2 * sc['resolution']) % 360
    } else if (sc['resolution'] > 3) {
      await tank.shoot(sc['angle'], d + 10)
      sc['resolution']--
      d = await tank.scan(sc['angle'], sc['resolution'])
      if (d === 0) {
        sc['encontrado'] = 0
        sc = await noTeEscapas(tank, sc, 8)
        while (sc['encontrado']) {
          if (sc['distance'] > 10) {
            await tank.shoot(sc['angle'], sc['distance'] + 10)
          }
          sc = await noTeEscapas(tank, sc, 8)
        }
        sc['resolution'] = 10
        await tank.shoot(sc['angle'], d + 10)
      }
    } else {
      await tank.shoot(sc['angle'], d + 10)
      sc['distance'] = d
      return sc
    }
  }
}

async function noTeEscapas (tank, sc, rafagas) {
  var d
  var mult = -1
  sc['encontrado'] = 0
  sc['distance'] = 0
  while (mult < rafagas) {
    var newAngle = (sc['angle'] + mult * sc['resolution']) % 360
    while (newAngle < 0) {
      newAngle += 360
    }
    d = await tank.scan(newAngle, sc['resolution'] + 1.5 * await abs(mult))
    if (d) {
      sc['angle'] = newAngle
      sc['resolution'] += await abs(mult)
      sc['encontrado'] = 1
      sc['distance'] = d
      break
    }
    if (mult > 0) {
      mult++
    }
    mult *= -1
  }
  return sc
}

async function main (tank) {
  while (true) {
    var sc = await superScan(tank)
    await tank.shoot(sc['angle'], sc['distance'])
  }
}

async function abs (num) {
  if (num < 0) {
    num *= -1
  }
  return num
}

async function shootAlways (tank, angle, distance) {
  var s = await tank.shoot(angle, distance)
  while (s === false) {
    s = await tank.shoot(angle, distance)
  }
}

async function defensaAbsoluta (tank) {
  var angle = 0
  while (true) {
    await shootAlways(tank, angle, 35)
    angle = (angle + 30) % 360
  }
}

// async function moveRandom (tank) {
//   var x = await tank.getX()
//   var y = await tank.getY()
//   var angle = await Math.floor(Math.random() * 10)
//   while (true) {
//     if (angle <= 5) {
//       while (await tank.getX() < 1240) {
//         await tank.drive(0, 100)
//         var angle_s = await Math.floor(Math.random() * 10)
//         await shootAlways(tank, angle_s, distance)
//       }
//       await tank.drive(0, 0)
//
//     } else {
//       while (await tank.getX() < 1240) {
//         await tank.drive(0, 100)
//       }
//       await tank.drive(0, 0)
//     }
//   }
// }
//
// async function tortuga (tank) {
//   var x = await tank.getX()
//   var y = await tank.getY()
//   if (x <= 670) {
//     if (y <= 500) {
//       await goTo(tank, 20, 20)
//     } else {
//       await goTo(tank, 20, 980)
//     }
//   } else {
//     if (y <= 500) {
//       await goTo(tank, 1320, 20)
//     } else {
//       await goTo(tank, 1320, 980)
//     }
//   }
// }
//
// async function goTo (tank, xDest, yDest) {
//   var x, y, varX, varY
//   x = await tank.getX()
//   y = await tank.getY()
//   varX = xDest - x
//   varY = yDest - y
//   while ((await abs(varX) > 50) || (await abs(varY) > 50)) {
//     x = await tank.getX()
//     y = await tank.getY()
//     varX = xDest - x
//     varY = yDest - y
//     console.log('' + Math.PI + ' ' + varX + ' ' + varY + ' ' + await abs(varY / varX) + ' ' + await Math.acos(await abs(varY / varX)))
//     await tank.drive((await Math.acos(varY / varX) / (2 * Math.PI)) * 360, 49)
//   }
// }
