var xGet = 0;
var yGet = 0;
var speedGet; 
var damageGet;

var speed = 100;
var damage = 0;
var distance = 0;
var angle = 0;
var miss = 0;
var angleS = 0;


var DIMX = 1340;
var DIMY = 1000;


async function get_rand(min, max) {
	var num = Math.random();
	num = num*(max-min) + min;
	return num;
}	


async function move(tank) {
	await tank.drive(angle, speed);
}

/*
async function turn(tank, angleO, angleN, speedN) {
	await tank.drive(angleO, 0);
	console.log(speedN);
	await tank.drive(angleN, speedN);
	console.log('aca');
	angle = angleN;
	speed = speedN;
}
*/


async function getAll(tank) {
	xGet = await tank.getX();
	yGet = await tank.getY();
	speedGet = await tank.getSpeed();
	damageGet = await tank.getDamage();
}



async function desp(tank) {
	var angleN;
	var angleOld;
	var dagmageOld;
	await move(tank);
	var damageOld = damage;
	await getAll(tank);
	var angleI;
	
	angleOld = angleS;
	angleS = await get_rand(angle - 45, angle + 45);
	if (xGet > 3*DIMX/4) {
		angleS = await get_rand(90-45, 270+45);
	}
	if (xGet < 1*DIMX/4) {
		
		angleS = await get_rand(270-45, 450+45) % 360;
	}
	if (yGet > 3*DIMY/4) {
		angleS = await get_rand(180-45, 359-45);
	}
	if (yGet < 1*DIMX/4) {
		angleS = await get_rand(360-45, 360+180+45) % 360;
	}
	
	if (xGet > 3*DIMX/4 && yGet > 3*DIMY/4) {
		angleS = await get_rand(180-45, 270+45);
	}
	if (xGet < 1*DIMX/4 && yGet > 3*DIMY/4) {
		angleS = await get_rand(270-45, 360+45);
	}
	if (xGet > 3*DIMX/4 && yGet < 1*DIMY/4) {
		angleS = await get_rand(90-45, 180+45);
	}
	if (xGet < 1*DIMX/4 && yGet < 1*DIMY/4) {
		angleS = await get_rand(360-45, 360+90+45) % 360;
	}  
	if(distance != 0) {
		angleS = angleOld;
	}
	/*
	if(damageOld != damage || (xGet % 3 == 0 && yGet %3 == 0)) {
		angleS = angle;
	}
	*/
	distance = await tank.scan(angleS, 10);
	
	
	
	speed = 50;
	
	if (distance != 0 && distance <= 700) {
		miss = await tank.shoot(angleS, distance);
	}
	
	else if (xGet % 12 == 0) {
		angleI = await get_rand(300, 600);
		miss = await tank.shoot(angleS, angleI);
	}
	
	
	if( distance < 400 && distance != 0) {
		speed = 50;
		angle = await get_rand(angle - 90, angle + 90);
		await move(tank);
	}
	
	else if (xGet > (DIMX - 300)) {
		speed = 50;
		angle = await get_rand(90, 270);
		await move(tank);
	}
	else if(yGet > (DIMY - 300)) {
		speed = 50;
		angle = await get_rand(180, 359);
		await move(tank);
	}
	else if (xGet < 300) {
		speed = 50;
		angle = await get_rand(360-90, 360+90) % 360;
		await move(tank);
	}
	else if (yGet < 300) {
		speed = 50;
		angle = await get_rand(0, 180);
		await move(tank);
	}
	else if( (xGet % 2 == 0) && (yGet % 3 == 0) ){
		speed = 50;
		angle = await get_rand(0, 359);
		await move(tank);		
	}
	else {
		speed = 100;
		await move(tank);
	}
}

	
async function main(tank) {
  // escribe tu programa aqui
  angle = 0;
  angleS = angle;
  speed = 50;
  distance = 0;
  
  while(true) {
	await desp(tank);
  }
}
