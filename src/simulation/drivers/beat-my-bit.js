async function moverse(tank, angulo){
	let x = await tank.getX()
	if (x < 200){
		await tank.drive(0,70)
		return
	}
	if (x > 1130){
		await tank.drive(180,70)
		return
	}
	let y = await tank.getY()
	if (y < 200){
		await tank.drive(90,70)
		return
	}
	if (y > 800){
		await tank.drive(270,70)
		return
	}
	await tank.drive(angulo, 50)
	return
}

async function perseguir(tank, angulo, incremento, amplitud){
	let d1 = await tank.scan(angulo+incremento, amplitud)
	if (d1 > 0){
		await tank.shoot(angulo+incremento, d1)
		if (d1 < 50) 
			await moverse(tank, angulo+incremento+180)
		else
			await moverse(tank, angulo+incremento)
//		await tank.drive(angulo+incremento, 50)
		perseguir(tank, angulo+incremento, incremento, amplitud)
	}
	let d2 = await tank.scan(angulo-incremento, amplitud)
	if (d2 > 0){
		await tank.shoot(angulo-incremento, d2)
		if (d2 < 50)
			await moverse(tank, angulo-incremento+180)
		else
			await moverse(tank, angulo-incremento)
//		await tank.drive(angulo-incremento, 50)
		perseguir(tank, angulo-incremento, incremento, amplitud)
	}
	let d3 = await tank.scan(angulo+2*incremento, amplitud)
	if (d3 > 0){
		await tank.shoot(angulo+2*incremento, d3)
		if (d3 < 50)
			await moverse(tank, angulo+2*incremento+180)
		else
			await moverse(tank, angulo+2*incremento)
//		await tank.drive(angulo+2*incremento, 50)
		perseguir(tank, angulo+2*incremento, incremento, amplitud)
	}
	let d4 = await tank.scan(angulo-2*incremento, amplitud)
	if (d4 > 0){
		await tank.shoot(angulo-2*incremento, d4)
		if (d4 < 50)
			await moverse(tank, angulo-2*incremento+180)
		else
			await moverse(tank, angulo-2*incremento)
//		await tank.drive(angulo-2*incremento, 50)
		perseguir(tank, angulo-2*incremento, incremento, amplitud)
	}
	apuntar_y_disparar(tank)
}

async function apuntar_y_disparar(tank){
	let incremento = 8
	let flag = 0
	let angulo = Math.random()*360
	while (true){
		if (flag == 6){
			angulo += 60
			flag = 0
		}
		let d = await tank.scan(angulo, 10)
		await moverse(tank, angulo)
		if (d > 0){	
			await perseguir(tank, angulo, incremento, 10)
		} else{
			flag+=1
		}
		angulo += incremento
	}
}

async function main(tank) {
	await apuntar_y_disparar(tank);
}
