var zona, cont=0;
 
async function FindAndShoot(tank, from, to){
    let increment = 4;
    for (let i = from; i <= to; i = i + increment){
        let distance = await tank.scan(i - (increment/2), increment);
        while(distance > 100 && distance <= 700){
            await tank.shoot(i - (increment/2), distance);
            distance = await tank.scan(i - (increment/2), increment);
        }
   }
}
 
async function SeekAndDestroy(tank){
    let distance = await tank.scan(zona*90, 5);
        if(zona==1){
            if(distance>0){
                await tank.shoot(0+(zona)*90, distance);
                await tank.shoot(5+(zona)*90, distance*1.1);
            }
             
        }
        else if(zona==2){
            if(distance>0){
                await tank.shoot(0+(zona)*90, distance);
                await tank.shoot(5+(zona)*90, distance*1.1);
            }
             
        }
        else if(zona==3){
            if(distance>0){
                await tank.shoot(0+(zona)*90, distance);
                await tank.shoot(5+(zona)*90, distance*1.1);
            }
             
        }
        else if(zona==4){           
            if(distance>0){
                await tank.shoot(0+(zona)*90, distance);
                await tank.shoot(5+(zona)*90, distance*1.1);
            }
            
        }
     
}

async function dispara(tank, angle){
    let distance = await tank.scan(angle, 5);

            if(distance>0){
                await tank.shoot(0+angle, distance);
                await tank.shoot(5+angle, distance*1.1);
            }
}
 
async function main(tank) {
 
  await tank.shoot(53,550);
  await tank.shoot(37,500);
 
   if(await tank.getX()<500){
    zona=3;
  }else if(await tank.getX()>800){
    zona=1;
  }else if(await tank.getY()<500){
    zona=4;
  }else{
    zona=2;
  }
 
  while (true){ 
      
    if(zona==4){
        await tank.drive(0, 70);
        await SeekAndDestroy(tank);
        while(await tank.getX() < 900){
        	cont=cont+30;
        	await dispara(tank, zona*90+cont);
        }
        cont=0;
        await tank.drive(0,0);
        //await FindAndShoot(tank, 90, 180);
        zona=1;
    }
   
    if(zona==1){
        await tank.drive(90, 70);
        await SeekAndDestroy(tank);
        while(await tank.getY() < 700){
        	cont=cont+30;
        	await dispara(tank, zona*90+cont);
        }
        cont=0;
        await tank.drive(0,0);
        //await FindAndShoot(tank, 180, 270);
        zona=2;
    }
    if(zona==2){ 
        await tank.drive(180, 70);
        await SeekAndDestroy(tank);
        while(await tank.getX() > 300){
        	cont=cont+30;
        	await dispara(tank, zona*90+cont);
        }
        cont=0;
        await tank.drive(0,0);
        //await FindAndShoot(tank, 270, 360);
        zona=3;
    }
    if(zona==3){
        await tank.drive(270, 70);        
        await SeekAndDestroy(tank);
        while(await tank.getY() > 300){
        	cont=cont+30;
        	await dispara(tank, zona*90+cont);
        }
        cont=0;
        await tank.drive(0,0);
        zona=4;
    }
  }
}