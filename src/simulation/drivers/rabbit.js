/* rabbit */
/* rabbit runs around the field, randomly */
/* and never fires;  use as a target */

function rand(n) { return Math.floor(Math.random() * n) }
const RAD_TO_DEG = 180 / Math.PI

async function go(tank, destX, destY) {
  let curX = await tank.getX()
  let curY = await tank.getY()
  while(distance(await tank.getX(), await tank.getY(), destX, destY) > 250) {
    let curX = await tank.getX()
    let curY = await tank.getY()
    let dist = distance(curX, curY, destX, destY)
    let course = plotCourse(curX, curY, destX, destY)
    await tank.drive(course, 100)
  }
  while (await tank.getSpeed() > 50)
    await tank.drive(0, 0)
}

function distance(x1, y1, x2, y2) {
  let x = x1 - x2
  let y = y1 - y2
  return Math.sqrt((x * x) + (y * y))
}

function plotCourse(curX, curY, xx, yy) {
  let x = curX - xx
  let y = curY - yy

  if (x == 0) {
    if (yy > curY)
      d = 90
    else
      d = 270
  } else {
    if (yy < curY) {
      if (xx > curX)
        d = 360 + RAD_TO_DEG * Math.atan(y / x)
      else
        d = 180 + RAD_TO_DEG * Math.atan(y / x)
    } else {
      if (xx > curX)
        d = RAD_TO_DEG * Math.atan(y / x)
      else
        d = 180 + RAD_TO_DEG * Math.atan(y / x)
    }
  }
  return d
}

async function main(tank) {
  while(true) {
    await go(tank, rand(1000), rand(1000))
  }
}
