async function main(tank) {
  let range = 0
  let x = 0
  while(true) {
    if (await tank.scan(x, 10) != 0) {
      x += 5 - Number(await tank.scan(x - 5, 5) != 0) * 10;
      x += 3 - Number(await tank.scan(x - 3, 3) != 0) * 6;
      if ((range = await tank.scan(x, 10)) > 20) {
        await tank.shoot(x, range)
        if (range > 200)
          await tank.drive(x, 50)
        else
          await tank.drive(180 - x, 0)
      }
      x -= 20
    } else {
      x += 20
    }
  }
}
