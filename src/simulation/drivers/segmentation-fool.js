async function main(tank) {
  let distance;
  let angle;
  let angle2;
  let x_pos;
  let y_pos;
  let ite;
  let previa;
  let actual;
  let angulo_ev;
  let x_ev;
  let y_ev;
  ite = 0;
  actual = await tank.getDamage();
  previa = actual;
  while(true){
	x_pos = await tank.getX();
	y_pos = await tank.getY();
	if(ite==0){
		if( x_pos > 1100){
			angle = 180;
		}else if( x_pos < 200){
			angle = 0;
		}else if(y_pos > 800){
			angle = 270;
		}else if(y_pos < 200){
			angle = 90; 		
		}else{
			angle = Math.random()*360;
		}
	}
	await tank.drive(angle,50);
	ite = ite +1;
	if (ite == 10){
		ite = 0;
	}
    [distance,angle2] = await rastrear_random(tank,10);
    if(distance > 0){
		await tank.shoot(angle2,distance);
    	await tank.shoot(angle2+5,distance);
        await tank.drive(angle,40);
    }
	actual = await tank.getDamage();
	if(actual > previa){
		//Evasion
		x_ev = Math.random()*(900-100)+100;
        y_ev = Math.random()*(1100-100)+100;
		angulo_ev = Math.atan((x_ev-x_pos)/(y_ev-y_pos));
		await tank.drive(angulo_ev,80);	
   }
previa = actual;
  }
}
async function rastrear_random(tank, precision){
	var angulo = Math.random() * (360);
	return [await tank.scan(angulo, precision), angulo];
}