async function modoHardcore(tank){
	let angle=0;

	while (true){
		angle=25;
		while (await tank.getX()<1100){
			dist =await tank.scan(angle, 7);
			if (dist==0){
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, 400);
				else await tank.shoot(25, 400);
				if (angle>=360) angle-=360;
				angle+=20;
			}
			else{
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, dist);
				else await tank.shoot(25, dist);
				if (angle>=360) angle-=360;
				angle+=20;
			}
		}

		await tank.drive(0, 0);
		await tank.drive(105, 20);
		await tank.drive(105, 75);
		angle=105;
		while (await tank.getY()<750){
			dist =await tank.scan(angle, 7);
			if (dist==0){
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, 400);
				else await tank.shoot(105, 400);
				if (angle>=360) angle-=360;
				angle+=20;}
			else{
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, dist);
				else await tank.shoot(105, dist);
				if (angle>=360) angle-=360;
				angle+=20;
			}
		}


		await tank.drive(0, 0);
		await tank.drive(215, 20);
		await tank.drive(215, 75);
		angle=215;
		while (await tank.getX()>300){
			dist =await tank.scan(angle, 7);
			if (dist==0){
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, 400);
				else await tank.shoot(215, 400);
				if (angle>=360) angle-=360;
				angle+=20;
			}
			else{
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, dist);
				else await tank.shoot(215, dist);
				if (angle>=360) angle-=360;
				angle+=20;
			}
		}

		await tank.drive(0, 0);
		await tank.drive(305, 20);
		await tank.drive(305, 75);
		angle=305;
		while (await tank.getY()>300){
			dist =await tank.scan(angle, 7);
			if (dist==0){
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, 400);
				else await tank.shoot(305, 400);
				if (angle>=360) angle-=360;
				angle+=20;
			}
			else{
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, dist);
				else await tank.shoot(305, dist);
				if (angle>=360) angle-=360;
				angle+=20;
			}
		}

		await tank.drive(0, 0);
		await tank.drive(25, 20);
	}
}

async function main(tank) {
	let vida;
	let angle=0;
	let dist;

	while (true){
		vida=100-(await tank.getDamage());
		if (vida<=40) await modoHardcore(tank);

		if (await tank.getX()<100) await tank.drive(5, 75);
		else await tank.drive(25, 75);
		angle=25;

		while (await tank.getX()<1100){
			dist =await tank.scan(angle, 7);
			if (dist==0){
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, 400);
				else await tank.shoot(25, 400);
				if (angle>=360) angle-=360;
				angle+=20;
			}
			else{
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, dist);
				else await tank.shoot(25, dist);
				if (angle>=360) angle-=360;
				angle+=20;
			}
		}

		await tank.drive(0, 0);
		await tank.drive(105, 20);
		await tank.drive(105, 75);
		angle=105;
		while (await tank.getY()<750){
			dist =await tank.scan(angle, 7);
			if (dist==0){
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, 400);
				else await tank.shoot(105, 400);
				if (angle>=360) angle-=360;
				angle+=20;}
			else{
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, dist);
				else await tank.shoot(105, dist);
				if (angle>=360) angle-=360;
				angle+=20;
			}
		}


		await tank.drive(0, 0);
		await tank.drive(215, 20);
		await tank.drive(215, 75);
		angle=215;
		while (await tank.getX()>300){
			dist =await tank.scan(angle, 7);
			if (dist==0){
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, 400);
				else await tank.shoot(215, 400);
				if (angle>=360) angle-=360;
				angle+=20;
			}
			else{
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, dist);
				else await tank.shoot(215, dist);
				if (angle>=360) angle-=360;
				angle+=20;
			}
		}

		await tank.drive(0, 0);
		await tank.drive(305, 20);
		await tank.drive(305, 75);
		angle=305;
		while (await tank.getY()>300){
			dist =await tank.scan(angle, 7);
			if (dist==0){
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, 400);
				else await tank.shoot(305, 400);
				if (angle>=360) angle-=360;
				angle+=20;
			}
			else{
				if (await tank.scan(angle, 7)) await tank.shoot(angle+5, dist);
				else await tank.shoot(305, dist);
				if (angle>=360) angle-=360;
				angle+=20;
			}
		}

		await tank.drive(0, 0);
		await tank.drive(25, 20);
	}
}
