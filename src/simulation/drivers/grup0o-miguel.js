//La arena es un rectángulo de 1340x1000 
//El origen (0, 0) es la esquina inferior izquierda 
//El ángulo 0º es el Este y el ángulo 90º es el Norte

async function main(tank) {
  // escribe tu programa aqui
  var direcciones = [0 ,90, 270 ,135, 315, 45, 225, 180];
  margenX = 300;
  margenY = 300;
  margenSpeed = 30; 
  limSpeed = 40;
  lim_sup_X = 700;//1340;
  lim_inf_X = 700;//0;
  lim_sup_Y = 700;//1000;
  lim_inf_Y = 400;//0;
  dir =0;
  resol = 5;
  dist_tank = 400;
  
  await tank.drive(0,100);
  aimDir =90;
  resolAim = 10;
 
  while(1){
	/*console.log(await tank.getX());      // 461 
	console.log(await tank.getY());      // 789
	console.log(await tank.getSpeed());  // 0 
	console.log(await tank.getDamage()); // 0*/
	
	col = await tank.scan(direcciones[dir], resol);
	colision = (col < dist_tank && col!=0);
	console.log(colision);
	
	switch(dir){
		case 0://va derecha
			limiteX = Math.floor((Math.random() * margenX)+lim_sup_X);
			if(await tank.getX()>limiteX || colision==1){
				dir = Math.floor((Math.random() * 6)+1);
				
				console.log(dir);
				speed = Math.floor((Math.random() * margenSpeed)+limSpeed);
				await tank.drive(direcciones[dir],speed);
			}
			break;
		case 1://va arriba
		limiteY = Math.floor((Math.random() * margenY)+lim_sup_Y);
		if(await tank.getY()>limiteY || colision==1){
				dir = Math.floor((Math.random() * 7));
				if(dir==1){
						dir=2;
				}
				console.log(dir);
				speed = Math.floor((Math.random() * margenSpeed)+limSpeed);
				await tank.drive(direcciones[dir],speed);
			}
		break;
		case 2:// va abajo
			limiteY = lim_inf_Y - Math.floor((Math.random() * margenY));
			if(await tank.getY()<limiteY || colision==1){
					dir = Math.floor((Math.random() * 7));
					if(dir==2){
						dir=3;
					}
					console.log(dir);
					speed = Math.floor((Math.random() * margenSpeed)+limSpeed);
					await tank.drive(direcciones[dir],speed);
			}
		break;
		case 3://va arriba izquierda
			limiteX = lim_inf_X - Math.floor((Math.random() * margenX));
			limiteY = Math.floor((Math.random() * margenY)+lim_sup_Y);
			if(await tank.getY()>limiteY || await tank.getX()<limiteX || colision==1){
					if(await tank.getY()>limiteY){
						dir = Math.floor((Math.random() * 7));
						if(dir==3 || dir==1){
								dir=0;
						}	
					}
					else{
						dir = Math.floor((Math.random() * 7));
						if(dir==3 || dir==7){
								dir=1;
						}	
					}
					
					console.log(dir);
					speed = Math.floor((Math.random() * margenSpeed)+limSpeed);
					await tank.drive(direcciones[dir],speed);
			}
		break;
		case 4://va abajo derecha
			limiteY = lim_inf_Y - Math.floor((Math.random() * margenY));
			limiteX = Math.floor((Math.random() * margenX)+lim_sup_X);
			if(await tank.getY()<limiteY || await tank.getX()>limiteX || colision==1){
					if(await tank.getY()<limiteY){
						dir = Math.floor((Math.random() * 7));
						if(dir==4 || dir==2){
								dir=7;
						}	
					}
					else{
						dir = Math.floor((Math.random() * 7));
						if(dir==4 || dir==0){
								dir=0;
						}	
					}
					
					console.log(dir);
					speed = Math.floor((Math.random() * margenSpeed)+limSpeed);
					await tank.drive(direcciones[dir],speed);
			}
		break;
		case 5: //va arriba derecha
			limiteY = Math.floor((Math.random() * margenY)+lim_sup_Y);
			limiteX = Math.floor((Math.random() * margenX)+lim_sup_X);
			if(await tank.getY()>limiteY || await tank.getX()>limiteX || colision==1){
					if(await tank.getY()>limiteY){
						dir = Math.floor((Math.random() * 7));
						if(dir==5 || dir==1){
								dir=7;
						}	
					}
					else{
						dir = Math.floor((Math.random() * 7));
						if(dir==5 || dir==0){
								dir=2;
						}	
					}
					
					console.log(dir);
					speed = Math.floor((Math.random() * margenSpeed)+limSpeed);
					await tank.drive(direcciones[dir],speed);
			}
		break;
		case 6: // abajo izquierda
			limiteY = lim_inf_Y - Math.floor((Math.random() * margenY));
			limiteX = lim_inf_X - Math.floor((Math.random() * margenX));
			if(await tank.getY()<limiteY || await tank.getX()<limiteX || colision==1){
					if(await tank.getY()<limiteY){
						dir = Math.floor((Math.random() * 7));
						if(dir==6 || dir==2){
								dir=5;
						}	
					}
					else{
						dir = Math.floor((Math.random() * 7));
						if(dir==6 || dir==7){
								dir=5;
						}	
					}
					
					console.log(dir);
					speed = Math.floor((Math.random() * margenSpeed)+limSpeed);
					await tank.drive(direcciones[dir],speed);
			}
			break;
		default:// va izquierda
			limiteX = lim_inf_X - Math.floor((Math.random() * margenX));
			if(await tank.getX()<limiteX || colision==1){
				dir = Math.floor((Math.random() * 6));
				console.log(dir);
				speed = Math.floor((Math.random() * margenSpeed)+limSpeed);
				await tank.drive(direcciones[dir],speed);
			}
			break;
	}
	
	/*aim = await tank.scan(aimDir, resolAim);
	if(aim==0){
		aimDir = aimDir+10;
	}
	if(aimDir>340){
		aimDir = 10;
	}
	if(aimDir == direcciones[dir]){
		aimDir = direcciones[dir+1];
	}
	await tank.shoot(aimDir+5, 700);
	await tank.shoot(aimDir-5, 700);*/
	
	await tank.shoot(direcciones[dir]+5, 700);
	await tank.shoot(direcciones[dir]-5, 700);
		
	
	//while(await tank.getX()< 600) null;
	
	
  }
  
}
