  var angle = 0;
  var distance = 0;
  var i = 0;
  var x = 0;
  var y = 0;
  var flag = 0;

function randomAngle(flag){
	return Math.random()*90 + flag*90;
}

async function dispara(tank, angulo, distancia){
	if(distancia < 30){
		return;
	}
	await tank.shoot(angulo, 50000);
	return;
}

async function inicia(tank){
	if(await tank.getX() > 760){
		while(true){
			angle = randomAngle(1);
			distance = await tank.scan(angle, 10);
			await dispara(tank, angle, distance);
			x = await tank.getX();
			y = await tank.getY();
			if(x > 200 && y > 200){
				await tank.drive(225, 100);
			}
			else{
				break;
			}
		}
		while(true){
			angle = randomAngle(1);
			distance = await tank.scan(angle, 10);
			await dispara(tank, angle, distance);
			y = await tank.getY();
			if(y > 130){
				await tank.drive(270, y/4);
			}
			else if(y < 100){
				await tank.drive(90, 50);
			}
			else{
				break;
			}
		}
		return;
	}
	while(true){
		angle = randomAngle(0);
		distance = await tank.scan(angle, 10);
		await dispara(tank, angle, distance);
		x = await tank.getX();
		y = await tank.getY();
		if(x > 200 && y > 350){
			await tank.drive(225, 100);
		}
		else{
			break;
		}
	}
	while(true){
		angle = randomAngle(0);
		distance = await tank.scan(angle, 10);
		await dispara(tank, angle, distance);
		x = await tank.getX();
		if(x > 130){
			await tank.drive(180, x/4);
		}
		else if (x < 100){
			await tank.drive(0, 50);
		}
		else{
			break;
		}
	}
	while(true){
		angle = randomAngle(0);
		distance = await tank.scan(angle, 10);
		await dispara(tank, angle, distance);
		y = await tank.getY();
		if(y > 130){
			await tank.drive(270, y/4);
		}
		else if(y < 100){
			await tank.drive(90, 50);
		}
		else{
			break;
		}
	}
}

async function main(tank) {
	await inicia(tank);
	while(true){
		while(await tank.getX() < 1160){
			await tank.drive(0,100); // ir pa la derecha
			distance = await tank.scan(90, 10);
			if(distance){
				flag = 0;
				await dispara(tank, 100, distance);
				// await tank.shoot(100, 50000);
			}
			else{
				flag++;
			}
			if(flag > 5){
				flag = 0;
				await dispara(tank, randomAngle(0) + 45, 500);
			}
		}
		for (i = 0; i < 30; i++) {
			await tank.drive(180,60);
		}
		while(await tank.getY() < 820){
			await tank.drive(90, 100); // ir parriba
			distance = await tank.scan(180, 10);
			if(distance){
				flag = 0;
				await dispara(tank, 190, distance);
				// await tank.shoot(190, 50000);
			}
			else{
				flag++;
			}
			if(flag > 5){
				flag = 0;
				await dispara(tank, randomAngle(0) + 135, 500);
			}
		}
		for (i = 0; i < 30; i++) {
			await tank.drive(270,60);
		}
		while(await tank.getX() > 180){
			await tank.drive(180,100); // ir pa la izquierda
			distance = await tank.scan(270, 10);
			if(distance){
				flag = 0;
				await dispara(tank, 280, distance);
				// await tank.shoot(280, 50000);
			}
			else{
				flag++;
			}
			if(flag > 5){
				flag = 0;
				await dispara(tank, randomAngle(0) + 225, 500);
			}
		}
		for (i = 0; i < 30; i++) {
			await tank.drive(0,60);
		}
		while(await tank.getY() > 180){
			await tank.drive(270, 100); // ir pabajo
			distance = await tank.scan(0, 10);
			if(distance){
				flag = 0;
				await dispara(tank, 10, distance);
				// await tank.shoot(10, 50000);
			}
			else{
				flag++;
			}
			if(flag > 5){
				flag = 0;
				await dispara(tank, randomAngle(0) + 315, 500);
			}
		}
		for (i = 0; i < 30; i++) {
			await tank.drive(90,60);
		}
	}
}
