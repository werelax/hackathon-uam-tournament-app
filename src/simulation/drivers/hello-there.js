async function move(tank, x, y, cambio){
    var x_inf = 300;
    var x_sup = 1050;
    var y_inf = 250;
    var y_sup = 750;
    var vel_min = 49;
    var vel_max = 100;
    var shoot_range = 250;
    var resol = 8;
    var distance;

    if(x < x_inf && y < y_inf){ // 1
        await tank.drive(0+cambio, vel_min);
        if(distance = await tank.scan(90, resol)){
            await tank.shoot(90, distance+100);
        }
        else if(distance = await tank.scan(0, resol)){
            await tank.shoot(0, distance+100);
        }
        else{
            await tank.shoot(45+cambio, shoot_range);
        }
    }
    else if(x > x_sup && y < y_inf){ // 2
        await tank.drive(90+cambio, vel_min);
        if(distance = await tank.scan(180, resol)){
            await tank.shoot(180, distance+100);
        }else if(distance = await tank.scan(90, resol)){
            await tank.shoot(90, distance+100);
        }else{
            await tank.shoot(135+cambio, shoot_range);
        }
    }
    else if(x > x_sup && y > y_sup){ // 3
        await tank.drive(180+cambio, vel_min);
        if(distance = await tank.scan(270, resol)){
            await tank.shoot(270, distance+100);
        }else if(distance = await tank.scan(180, resol)){
            await tank.shoot(180, distance+100);
        }
        else{
            await tank.shoot(225+cambio, shoot_range);
        }
    }
    else if(x < x_inf && y > y_sup){ // 4
        await tank.drive(270+cambio, vel_min);
        if(distance = await tank.scan(0, resol)){
            await tank.shoot(0, distance+100);
        }
        else if(distance = await tank.scan(270, resol)){
            await tank.shoot(270, distance+100);
        }
        else{
            await tank.shoot(315+cambio, shoot_range);
        }
    }
    else if(x > x_inf && y < y_inf){ // 5
        await tank.drive(0+cambio, vel_max);
        if(distance = await tank.scan(180, resol)){
            await tank.shoot(180, distance+100);
        }
        else if(distance = await tank.scan(0, resol)){
            await tank.shoot(0, distance+100);
        }
        else{
            await tank.shoot(90+cambio, shoot_range);
        }
    }
    else if(x > x_sup && y < y_sup){ // 6
        await tank.drive(90+cambio, vel_max);
        if(distance = await tank.scan(270, resol)){
            await tank.shoot(270, distance+100);
        }else if(distance = await tank.scan(90, resol)){
            await tank.shoot(90, distance+100);
        }else{
            await tank.shoot(180+cambio, shoot_range);
        }
    }
    else if(x > x_inf && y > y_sup){ // 7
        await tank.drive(180+cambio, vel_max);
        if(distance = await tank.scan(0, resol)){
            await tank.shoot(0, distance+100);
        }else if(distance = await tank.scan(180, resol)){
            await tank.shoot(180, distance+100);
        }
        else{
            await tank.shoot(270+cambio, shoot_range);
        }
    }
    else if(x < x_inf && y > y_inf){ // 8
        await tank.drive(270+cambio, vel_max);
        if(distance = await tank.scan(90, resol)){
            await tank.shoot(90, distance+100)
        }
        else if(distance = await tank.scan(270, resol)){
            await tank.shoot(270, distance+100);
        }
        else{
            await tank.shoot(0+cambio, shoot_range);
        }
    }else{
        if(y < 500){
            await tank.drive(270+cambio, vel_min);
        }
        else{
            await tank.drive(90+cambio, vel_min);
        }
        await tank.shoot(Math.random()*360, shoot_range);
    }
}

async function main(tank) {
    while(true){
        await move(tank, await tank.getX(), await tank.getY(), 0);
    }

}
