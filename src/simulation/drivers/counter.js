/* counter */
/* scan in a counter-clockwise direction (increasing degrees) */
/* moves when hit */

function rand(n) { return Math.floor(Math.random() * n) }

/* run moves around the center of the field */

let lastDir = 0

async function run(tank) {
  let x = await tank.getX()
  let y = await tank.getY()
  const amount = 250

  if (lastDir == 0) {
    lastDir = 1
    if (y > 512) {
      await tank.drive(270, 100)
      while (y - amount < await tank.getY())
        void 0
      await tank.drive(270, 0)
    } else {
      await tank.drive(90, 100)
      while (y + amount > await tank.getY())
        void 0
      await tank.drive(90, 0)
    }
  } else {
    lastDir = 0
    if (x > 512) {
      await tank.drive(180, 100)
      while (x - amount < await tank.getX())
        void 0
      await tank.drive(180,0)
    } else {
      await tank.drive(0,100)
      while (x + amount > await tank.getX())
        void 0
      await tank.drive(0, 0)
    }
  }
}

async function main(tank) {
  let res = 10
  let d = await tank.getDamage()
  let angle = rand(360)
  let range = 0
  while(true) {
    while ((range = await tank.scan(angle, res)) > 0) {
      if (range > 700) {
        /* out of range, head toward it */
        await tank.drive(angle, 50)
        /* use a counter to limit move time */
        for (let i = 0; i < 10; i++) await tank.scan(angle, res)
        await tank.drive(angle, 0)
        if (d !== await tank.getDamage()) {
          d = await tank.getDamage()
          await run(tank)
        }
        angle -= 3
      } else {
        await tank.shoot(angle, range)
        await tank.shoot(angle, range)
        if (d !== await tank.getDamage()) {
          d = await tank.getDamage()
          await run(tank)
        }
        angle -= 15
      }
    }
    if (d !== await tank.getDamage()) {
      d = await tank.getDamage()
      await run(tank)
    }
    angle += res
    angle %= 360
  }
}
