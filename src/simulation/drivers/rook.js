/* rook.r  -  scans the battlefield like a rook, i.e., only 0,90,180,270 */
/* move horizontally only, but looks horz and vertically */

let course = 0
let boundary = 0
let d = 0
let resolution = 10

/* look somewhere, and fire cannon repeatedly at in-range target */
async function look(tank, deg) {
  let range = 0
  while ((range = await tank.scan(deg, resolution)) > 0 && range <= 700)  {
    await tank.drive(course, 0)
    await tank.shoot(deg, range)
    if (d + 20 <= await tank.getDamage()) {
      d = await tank.getDamage()
      await change(tank)
    }
  }
}

async function change(tank) {
  if (course == 0) {
    boundary = 150
    course = 180
  } else {
    boundary = 850
    course = 0
  }
  await tank.drive(course, 30)
}

async function main(tank) {
  let y = 0

  /* move to center of board */
  if (await tank.getY() < 500) {
    /* start moving */
    await tank.drive(90, 50)
    /* stop near center */
    while (await tank.getY() - 200 < 50 && await tank.getSpeed() > 0)
      void 0
  } else {
    /* start moving */
    await tank.drive(270, 50)
    /* stop near center */
    while (await tank.getY() - 200 > 50 && await tank.getSpeed() > 0)
      void 0
  }
  await tank.drive(y, 0)

  /* initialize starting parameters */
  d = await tank.getDamage()
  course = 0
  boundary = 850
  await tank.drive(course, 70)

  /* main loop */

  while (1) {
    /* look all directions */
    await look(tank, 0)
    await look(tank, 90)
    await look(tank, 180)
    await look(tank, 270)

    /* if near end of battlefield, change directions */
    if (course === 0) {
      if (await tank.getX() > boundary || await tank.getSpeed() == 0)
        await change(tank)
    }
    else {
      if (await tank.getX() < boundary || await tank.getSpeed() == 0)
        await change(tank)
    }
  }
}
