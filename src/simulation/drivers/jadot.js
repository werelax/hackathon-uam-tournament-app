var N = 90
var E = 0
var S = 270
var W = 180
var mX = 0
var MX = 1340
var mY = 0
var MY = 1000
var speed = 100
var step = 1000
var STH = 0.9
var res = 10

async function goto_border(tank) {
  let x = await tank.getX()
  let y = await tank.getY()
  let y_angle = S
  let x_angle = W
  let angle = y_angle

  if (y > 0.5*MX) {
    y_angle = N
  } 
  if (x > 0.5*MX) {
    x_angle = E
  }

  if ( x < y ) {
    angle = x_angle
  }

  await tank.drive(angle,speed)
  return angle
}

async function dist_border(tank) {
  let x_0 = await tank.getX()
  let y_0 = await tank.getY()
  let x_1 = MX - x_0
  let y_1 = MY - y_0

  return await Math.min(x_0,y_0,x_1,y_1)
}
async function dist_edge(tank,angle) {
  switch (angle) {
    case S: return await tank.getY()
    case E: return  MX - await tank.getX()
    case N: return  MY - await tank.getY()
    case W: return await tank.getX()
  }
}

async function rotate(tank,angle,sign) {
  if (sign) {
    switch (angle) {
      case S: return E
      case E: return N
      case N: return W
      case W: return S
    }
  } else {
    switch (angle) {
      case S: return W
      case E: return S
      case N: return E
      case W: return N
    }
  }
}
async function invert(angle) {
  switch (angle) {
    case S: return N
    case E: return W
    case N: return S
    case W: return E
  }
}

async function angleToCenter(tank) {
    return Math.atan2(await tank.getY() - MY/2,await tank.getX() - MX/2) / 2 / Math.PI * 360 + 180
}


async function main(tank) {
  // escribe tu programa aqui
  let c = 0
  let thereIsEnemy = 0
  let sign = true
  let found = 0
  let seed = 0

  // go to closest wall
  let angle = await goto_border(tank)  
  while (await dist_border(tank) > (MX + MY)/2/4) {};
  await tank.drive(0,0)
  while (await tank.getSpeed() > 50) {}

  while (true) {

    // random hard turn
    if (await Math.random() > 0.5) {
      sign = !sign
      while (await dist_edge(tank,angle) > (MX + MY)/2/4) {};
      await tank.drive(0,0)
      angle = await invert(angle)
    }
    angle = await rotate(tank,angle,sign)
    await tank.drive(angle,speed)

    // untill wall is close
    while (await dist_edge(tank,angle) > (MX + MY)/2/4) {
      // look at the middle

      seed = Math.random()
      if (seed > 0.8) {
        angleShoot = angle + Math.random()*10
      } else if (seed < 0.2) {
        angleShoot =  angle -180 + Math.random()*10
        if (angleShoot < 0) {
          angleShoot = angleShoot + 360
        }
      } else {
        angleShoot = await angleToCenter(tank)
      }
      //console.log(angleShoot)
      dist = await tank.scan(angleShoot + Math.random(), 7.5 + (2.5* + Math.random()))
      if (dist != 0) {
        await tank.drive(0,0)
        await tank.shoot(angleShoot,dist)
        await tank.drive(angle,speed)
      }
    };
    await tank.drive(0,0)
    while (await tank.getSpeed() > 50) { }

  }

}
