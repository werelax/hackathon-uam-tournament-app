var angle = 0;
var last = 0;
var range = 150;
const PI = 3.141592;
let inicio = 1;

async function main(tank) {
 // await tank.drive(0, 75);
 // while (await tank.getX() < 800) {
 // null;
 // }
 // await tank.drive(0, 0);


 // while(true){
 //   console.log("   ----");
 //   console.log(tank);
 //   tank.lifePoints = 59;
 //     console.log("   ----");
 //   console.log(await tank.getX()); // 461
 //   console.log(await tank.getY()); // 789
 //   console.log(await tank.getSpeed()); // 0
 //   console.log(await tank.getDamage()); // 0
 // }

  if(inicio == 1) {
    let tx = await tank.getX();
    if(tx > 670){
      angle = 150;
    }else{
      angle = 330;
    }

    inicio = 0;
  }

  while(true){
    let addAng = 20;
     let dist = await tank.scan(angle, 10);
     if(await tank.getDamage() < 50){
       console.log("ataque");
       if(dist != 0){
         last = 1;
         let x = await tank.getX();
         let y = await tank.getY();
         await tank.shoot(angle, 700);
         if(x > range && x < 1340 - range && y > range && y < 1000 -  range){
           await tank.drive(angle, 60);
         }else{
           await salirDelBorde(tank);
           // await tank.drive(0,0);
         }
       }else if (last == 1) {
        angle -= addAng;
        angle = (angle >= 0 ? angle : (360 + angle));
        last = 0;
      }else if (last == 0) {
        angle += addAng;
        angle = angle % 360;
      }
     }else{
       console.log("esquinita");
       let esq = await esquinita(tank);
       console.log(esq);
       await shoot(tank, esq);

     }
  }
}

async function salirDelBorde(tank) {
  let x = await tank.getX();
  let y = await tank.getY();
  if(x < range){
    await tank.drive(0, 80);
  }else if (x > 1340 - range) {
    await tank.drive(180, 80);
  }else if (y < range) {
    await tank.drive(270, 80);
  }else if (y > 1000 - range) {
    await tank.drive(90, 80);
  }
}

async function esquinita(tank) {
  let x = await tank.getX();
  let y = await tank.getY();
  let destX;
  let destY;
  let esquinita;

  if(x > 670){
    destX = 1340 - range;
    if(y > 500){
      destY = 1000 - range;
      esquinita = 3;
    }else{
      destY = range;
      esquinita = 2;
    }
  }else{
    destX = range;
    if(y > 500){
      destY = 1000 - range;
      esquinita = 4;
    }else{
      destY = range;
      esquinita = 1;
    }
  }console.log(x, y, esquinita);

  let a = Math.atan2(destY - y, destX - x);
  a = (a > 0 ? a : (2*PI + angle)) * 360 / (2*PI);

  await tank.drive(a, 50);
  if(await dist(x, y, destX, destY) > range){
    await tank.drive(a, 80);
  }else {
    await tank.drive(0, 0);
  }
  console.log(a);
  return esquinita;
}

async function dist(x,y, a, b) {
  return Math.sqrt((x - a)*(x - a) + (y - b)*(y - b));
}

async function shoot(tank, q){
  let angle1, angle2;
	switch (q){
		case 1:

			angle1=0;
			angle2=90;


		break;
		case 2:

			angle1=90;
			angle2=180;
		break;

		case 3:

			angle1=180;
			angle2=270;
		break;

		case 4:

			angle1=270;
			angle2=360;

		break;
  }
  angle = angle1;
  if (angle>=angle1 && angle<=angle2){
    while(angle>=angle1&&angle<=angle2){
      angle+=10;

      if (await tank.scan(angle,10) != 0){
        angle+=10;
        if (await tank.scan(angle,10) != 0){
          await tank.shoot(angle+7,700);
          await tank.shoot(angle+7,700);
        }
        else{
          await tank.shoot(angle-25,700);
          await tank.shoot(angle-25,700);
        }
      }
    }
  }
  angle = angle % 360;
}
