async function goRight(tank) {
  await tank.shoot(0, 500)
  await tank.shoot(0, 300)
  while ((await tank.getPosition())[0] < 950) {
    await tank.drive(0, 100)
    await tank.shoot(0, 500)
  }
  await tank.shoot(180, 300)
  while ((await tank.getSpeed()) > 50) {
    await tank.drive(0, 0)
  }
}

async function goLeft(tank) {
  while ((await tank.getPosition())[0] > 400) {
    await tank.drive(180, 100)
    await tank.shoot(180, 700)
  }
  await tank.shoot(180, 700)
  while ((await tank.getSpeed()) > 50) {
    await tank.drive(180, 0)
  }
}

async function main(tank) {
  for (let i = 0; i < 360; i++) {
    const distance = await tank.scan(i, 10)
  }
  // while(true) {
  //   const [x, y] = await tank.getPosition()
  //   if (x < 500) {
  //     await goRight(tank)
  //   } else {
  //     await goLeft(tank)
  //   }
  // }
}
