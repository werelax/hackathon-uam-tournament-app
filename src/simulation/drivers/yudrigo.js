var par = 1;
var sentido = 1;
async function main(tank) {
    // escribe tu programa aqui
    var curr = await llegarapared(tank);
    while (true) {
        curr = await moverse(tank, curr);
        if (par == 1) {
            par = 0;
            curr = await reaccion(tank, curr);
        } else {
            par = 1;
            curr = await centro(tank, curr);
        }
    }
}
async function smartshoot(tank, near, angle, dist) {
    if (near > 80) {
        await tank.shoot((angle + 5) % 360, dist);
        await tank.shoot((angle - 5) % 360, dist);
        if (par == 1) par = 0;
        else par = 1;
    }
}
async function dobleshoot(tank, near, angle, dist) {
    if (near > 80) {
        await tank.shoot(angle, dist);
        await tank.shoot(angle, dist);
        if (par == 1) par = 0;
        else par = 1;
    }
}
async function centro(tank, curr) {
    var near;
    if (curr == 0) {
        if ((near = await tank.scan(45, 10)) > 0) {
            await smartshoot(tank, near, 45, 500);
        } else if ((near = await tank.scan(90, 10)) > 0) {
            await smartshoot(tank, near, 90, 500);
        } else if ((near = await tank.scan(135, 10)) > 0) {
            await smartshoot(tank, near, 135, 500);
        }
    } else if (curr == 1) {
        if ((near = await tank.scan(135, 10)) > 0) {
            await smartshoot(tank, near, 135, 500);
        } else if ((near = await tank.scan(180, 10)) > 0) {
            await smartshoot(tank, near, 180, 500);
        } else if ((near = await tank.scan(225, 10)) > 0) {
            await smartshoot(tank, near, 225, 500);
        }
    } else if (curr == 2) {

        if ((near = await tank.scan(270, 10)) > 0) {
            await smartshoot(tank, near, 270, 500);
        } else if ((near = await tank.scan(225, 10)) > 0) {
            await smartshoot(tank, near, 225, 500);
        } else if ((near = await tank.scan(315, 10)) > 0) {
            await smartshoot(tank, near, 315, 500);
        }
    } else {

        if ((near = await tank.scan(0, 10)) > 0) {
            await smartshoot(tank, near, 10, 500);
            await smartshoot(tank, near, 350, 500);
        } else if ((near = await tank.scan(45, 10)) > 0) {
            await smartshoot(tank, near, 45, 500);
        } else if ((near = await tank.scan(315, 10)) > 0) {
            await smartshoot(tank, near, 315, 500);
        }
    }
    return curr;
}
async function reaccion(tank, curr) {
    var near;
    if (curr == 0) {
        if ((near = await tank.scan(0, 6)) > 0) {
            if (sentido == 1) sentido = -1;
            await dobleshoot(tank, near, 0, 700);
        }
        else if ((near = await tank.scan(180, 6)) > 0) {
            if (sentido == -1) sentido = 1;
            await dobleshoot(tank, near, 180, 700);
        }

    } else if (curr == 1) {
        if ((near = await tank.scan(90, 6)) > 0) {
            if (sentido == 1) sentido = -1;
            await dobleshoot(tank, near, 90, 700);
        } else if ((near = await tank.scan(270, 6)) > 0) {
            if (sentido == -1) sentido = 1;
            await dobleshoot(tank, near, 270, 700);
        }

    } else if (curr == 2) {
        if ((near = await tank.scan(180, 6)) > 0) {
            if (sentido == 1) sentido = -1;
            await dobleshoot(tank, near, 180, 700);
        }
        else if ((near = await tank.scan(0, 6)) > 0) {
            if (sentido == -1) sentido = 1;
            await dobleshoot(tank, near, 0, 700);
        }

    } else {
        if ((near = await tank.scan(270, 6)) > 0) {
            if (sentido == 1) sentido = -1;
            await dobleshoot(tank, near, 270, 700);
        }
        else if ((near = await tank.scan(90, 6)) > 0) {
            if (sentido == -1) sentido = 1;
            await dobleshoot(tank, near, 90, 700);
        }
    }
    return curr;
}
function f(x) {
    if (x == 1) return 0;
    return 1;
}
async function moverse(tank, curr) {
    var y = await tank.getY();
    var x = await tank.getX();
    // sentido = 1;
    if (sentido == 1) {
        if (curr == 0) {
            if (x > 1150) {
                curr = (curr + sentido) % 4;
                await tank.drive(90, 0);
            } else {
                await tank.drive((0 + 180 * f(sentido)) % 360, 75);
            }
        } else if (curr == 1) {
            if (y > 800) {
                curr = (curr + sentido) % 4;
                await tank.drive(90, 0);
            } else {
                await tank.drive((90 + 180 * f(sentido)) % 360, 75);

            }
        } else if (curr == 2) {
            if (x < 200) {
                curr = (curr + sentido) % 4;

                await tank.drive(90, 0);
            } else {
                await tank.drive((180 + 180 * f(sentido)) % 360, 75);
            }
        } else {
            if (y < 200) {
                curr = (curr + sentido) % 4;

                await tank.drive(90, 0);
            } else {
                await tank.drive((270 + 180 * f(sentido)) % 360, 75);
            }
        }
    } else {
        if (curr == 0) {
            if (x < 200) {
                curr = 3;
                await tank.drive(90, 0);
            } else {
                await tank.drive((0 + 180 * f(sentido)) % 360, 75);
            }
        } else if (curr == 1) {
            if (y < 200) {
                curr = 0;
                await tank.drive(90, 0);
            } else {
                await tank.drive((90 + 180 * f(sentido)) % 360, 75);

            }
        } else if (curr == 2) {
            if (x > 1150) {
                curr = 1;
                await tank.drive(90, 0);
            } else {
                await tank.drive((180 + 180 * f(sentido)) % 360, 75);
            }
        } else {
            if (y > 800) {
                curr = 2;

                await tank.drive(90, 0);
            } else {
                await tank.drive((270 + 180 * f(sentido)) % 360, 75);
            }
        }
    }

    return curr;
}


async function llegarapared(tank) {
    var y = await tank.getY();
    var x = await tank.getX();

    if (y <= 1000 / 3) {
        await tank.shoot(90, 700);
        await tank.drive(270, 100);
        while (y > 140) {
            y = await tank.getY();
        }

        await tank.drive(270, 0);
        return 0;
        //abajo
    } else if (y >= 2000 / 3) {
        await tank.shoot(270, 700);
        await tank.drive(90, 100);
        while (y < 850) {
            y = await tank.getY();
        }
        await tank.drive(270, 0);
        // arriba
        return 2;
    } else if (x <= 1340 / 3) {
        await tank.shoot(0, 700);
        await tank.drive(180, 75);
        while (x > 180) {
            x = await tank.getX();
        }
        await tank.drive(270, 0);
        //izquierda
        return 3;
    } else {
        await tank.shoot(180, 700);
        await tank.drive(0, 75);
        while (x < 1200) {
            x = await tank.getX();
        }
        await tank.drive(270, 0);
        return 1;
        //Derecha
    }
}
