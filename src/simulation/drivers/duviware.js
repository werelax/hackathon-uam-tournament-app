let aimDirection = Math.random();
let foundDistance = 0;
let aimIncrement = 15;
let rangeError = 10;
let THRESHOLD = 200;
let MIN_DISTANCE = 75;
let xPositions = [200, 200, 400, 940, 1140, 1140, 940, 400]; //[100, 670, 1240]; //
let yPositions = [300, 700, 800, 800, 700, 300, 200, 200]; // [100, 900, 100];
let posIndex = 0;
let speed = 50;
let currentX;
let currentY;

var seekStates = {
  INITIALSEEK: 1,
  FOCUSSEEK: 2,
};

let state = seekStates.INITIALSEEK;

enemy = {x:0, y:0, distance:0, angle:0, range:0, time:new Date()};

async function main(tank) {
  enemy = null;
  await updateCoords();
  while(true){
    await moveTo(tank, currentX, currentY);
    var xpos = await tank.getX()
    if (xpos < 800){
      await tank.drive(0, 75);
    }
    switch (state){
      case seekStates.INITIALSEEK:
        enemy = await initialSeek(tank);
        if(enemy !== null)
          state = seekStates.FOCUSSEEK;
        break;
      case seekStates.FOCUSSEEK:
        let localEne;
        if (enemy == null)
          break;
        localEne = await focusSeek(tank, enemy);
        //console.log(localEne);
        if(localEne !== null){
            enemy = localEne;
            if (enemy.distance > MIN_DISTANCE) {
              await tank.shoot(enemy.angle, enemy.distance);
            }
        } else {
          state = seekStates.INITIALSEEK;
        }
        break;
    }
  }
}

async function getEnemy(tank, direction, range) {
  var dist = await tank.scan(direction, range);
  if(dist !== 0){
    return {
      x: await tank.getX(),
      y: await tank.getY(),
      distance: dist,
      angle: direction,
      range: range,
      time: new Date()
    };
  }
  else
    return null;
}
async function initialSeek(tank){
  //scan pseudo randomly
  aimDirection = (aimDirection + aimIncrement) % 360;
  return await getEnemy(tank, aimDirection, rangeError);
}

async function focusSeek(tank, enemy){
  var dirs = [0, 1, -1];
  var dirMean = 0;
  var distMean = 0;
  var n_enemies = 0;
  var ene = null;
  var range;

  for (var i = 0; i < 3; i++) {
    var direction = (enemy.angle + dirs[i]*aimIncrement*0.75) % 360;
    ene = await getEnemy(tank, direction, rangeError);
    //console.log("enemy: "+ene);
    /*
    if(ene !== null)
      console.log("is differenct: "+await isDifferentEnemy(ene, enemy, THRESHOLD));
      */
    if (ene !== null && await isDifferentEnemy(ene, enemy, THRESHOLD) == false) {
      dirMean += ene.angle;
      distMean += ene.distance + await error(25);
      n_enemies += 1;
      range = ene.range;
    }
  }
  //console.log("number of enemies: "+n_enemies);
  if (n_enemies <= 0)
    return null;

  return {
    x: await tank.getX(),
    y: await tank.getY(),
    distance: distMean/n_enemies,
    angle: dirMean/n_enemies,
    range: range,
    time: new Date()
  };
}

async function getEstimatedPos(enemy){
  var radians = enemy.angle * 2*Math.PI / 360;
  var x = enemy.x+enemy.distance*Math.cos(radians);
  var y = enemy.y+enemy.distance*Math.sin(radians);

  return [x, y];
}

async function isDifferentEnemy(enemy1, enemy2, threeShold){
  var dist = Math.abs(enemy1.distance-enemy2.distance);
  return dist > threeShold;
}

async function error(range){
  return (Math.random() * (range) - range/2);
}

async function distance(x1, y1, x2, y2){
  var a = x1 - x2;
  var b = y1 - y2;
  var c = Math.sqrt( a*a + b*b );
  return c;
}

async function moveTo(tank, x, y){
  var selfX = await tank.getX();
  var selfY = await tank.getY();
  var direction;
  console.log("x: "+x+" selfX: "+selfX+" y: "+y+" selfY: "+selfY);
  if(x !== selfX){
    direction = Math.atan((selfY-y)/(selfX-x))*180/Math.PI;
    if(x <= selfX)
      direction += 180;
  }
  else
    if(y > selfY)
      direction = 90;
    else
      direction = 270;

  direction = (direction +360) % 360;
  let dist = await distance(selfX, selfY, x, y);

  console.log("direction: "+direction);
  console.log("dist: "+dist);
  if(dist >= 100){
      await tank.drive(direction, speed);
  } else {
      await tank.drive(direction, speed*0.5);
      await updateCoords();
  }
}

async function updateCoords(){
  console.log("UPDATE COORSD");
  posIndex = (posIndex + 1) % xPositions.length;
  currentX = xPositions[posIndex];
  currentY = yPositions[posIndex];
}
