var angle_scan = 0;
var resolution_scan = 10;
var hit_flag = 0;
var change_angle_scan = 20;
var angle_movement = 45;
var speed_movement = 65;
var available_shoot = 5;


async function findWalls(tank,speed_current){
	let flag_x_east = false;
	let flag_x_west = false;
	let pos_x = await tank.getX() + speed_current*Math.cos(angle_movement);
	let pos_y = await tank.getY() + speed_current*Math.sin(angle_movement);
	let speed = 65;
	let angle = angle_movement;

	if(speed_movement == 0){
		tank.drive(angle, speed);
		return angle;
	}

	if((pos_x >= 1300 && (angle_movement < 90 || angle_movement > 270)) || (pos_x < 600 && pos_x > 300  && (angle_movement > 90 || angle_movement < 270)) ) {
		flag_x_east = true;
	}
	else if ((pos_x <= 200 && (angle_movement > 90 || angle_movement < 270)) || (pos_x > 800 && pos_x <1200  && (angle_movement > 90 || angle_movement < 270))) {
		flag_x_west = true;
	}
	if((pos_y <= 200 && angle_movement > 180) || (pos_y >500 && pos_y < 700 && angle_movement < 180)){
		if(flag_x_east){

			await tank.drive(0, 0);
			while(await tank.getSpeed() > 50){null;}
			angle = Math.random()*90+90;
			await tank.drive(angle, speed);
		}
		else if(flag_x_west){
			await tank.drive(0, 0);
			while(await tank.getSpeed() > 50){null;}
			
			angle = Math.random()*90;
			await tank.drive(angle, speed);
		}
		else{
			await tank.drive(0, 0);
			while(await tank.getSpeed() > 50){null;}
			
			angle = Math.random()*180;
			await tank.drive(angle, speed);
		}
	}
	else if((pos_y >= 800 && angle_movement < 180 )|| (pos_y >300 && pos_y < 500 && angle_movement < 180)){
		if(flag_x_east){
			await tank.drive(0, 0);
			while(await tank.getSpeed() > 50){null;}
			
			angle = Math.random()*90+180;
			await tank.drive(angle, speed);
		}
		else if(flag_x_west){
			await tank.drive(0, 0);
			while(await tank.getSpeed() > 50){null;}
			
			angle = Math.random()*90+270;
			await tank.drive(angle, speed);
		}
		else{
			await tank.drive(0, 0);
			while(await tank.getSpeed() > 50){null;}
			
			angle = Math.random()*180+180;
			await tank.drive(angle, speed);
		}
	}
	else if(flag_x_east){
		await tank.drive(0, 0);
		while(await tank.getSpeed() > 50){null;}
		if(Math.random >= 0.5){
			angle = Math.random()*90+90;
		}
		else{
			angle = Math.random()*90+180;
		}
		
		await tank.drive(angle, speed);
	}
	else if(flag_x_west){
		await tank.drive(0, 0);
		while(await tank.getSpeed() > 50){null;}
		if(Math.random >= 0.5){
			angle = Math.random()*90;
		}
		else{
			angle = Math.random()*90+270;
		}
		
		await tank.drive(angle, speed);
	}
	return angle;
}



async function scan_shoot(tank) {
	// Escaneo de otros tanques
	distance_tank = await tank.scan(angle_scan, resolution_scan);

	// Hit
	if (distance_tank != 0) {
		hit_flag=3;
		
		if (distance_tank < 200) {
			await tank.shoot(angle_scan, distance_tank);

			//Scape from other tanks
			await tank.drive(0, 0);
			while(await tank.getSpeed() > 50){null;}
			angle_movement = (angle_movement + (angle_scan+180 % 360))/2;
			speed_movement = 65;
			await tank.drive(angle_movement,speed_movement);

		} else if (resolution_scan < 6) {	
			await tank.shoot(angle_scan + resolution_scan, distance_tank);
			await tank.shoot(angle_scan - resolution_scan, distance_tank);
		}
		resolution_scan = Math.max(Math.ceil(resolution_scan / 2), 2);
		available_shoot = 3;

	// Nada
	} else {
		if (hit_flag > 0) {
			change_angle_scan = (-1) * change_angle_scan
			hit_flag--;
		}
		else{
			if(available_shoot == 0){
				await tank.shoot(Math.random()*360, 500);
				await tank.drive(angle_movement,speed_movement);
				available_shoot = 5;
			}
			else if (available_shoot == 3){
				await tank.shoot((angle_movement + 180)%360, 500);
				await tank.drive(angle_movement,speed_movement);
				available_shoot = available_shoot -1;
			}
			else{
				available_shoot = available_shoot -1;
			}

		}
		
		resolution_scan = Math.min(10, resolution_scan * 2);
		angle_scan = angle_scan + change_angle_scan;
		if (angle_scan > 360) angle_scan = 0;
		if (angle_scan < 0) angle_scan = 360;
	}
}





async function main(tank) {
	
	await tank.drive(angle_movement, speed_movement);
	while(true){
		angle_movement = await findWalls(tank,await tank.getSpeed());
		await scan_shoot(tank);
	}
	
}