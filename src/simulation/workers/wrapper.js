/* eslint-disable import/no-webpack-loader-syntax */

import prelude from '!raw-loader!./prelude.js'

export const workerWrapper = code => async (tank) => {
  // worker setup
  const blob = new Blob([
    prelude,
    code
  ])
  const blobUrl = window.URL.createObjectURL(blob)
  const worker = new Worker(blobUrl)
  // mesage handling
  let lastMessageStamp = Date.now()
  const handleMessage = async ({ data: { method, params } }) => {
    const now = Date.now()
    const delta = now - lastMessageStamp
    lastMessageStamp = now
    tank.workerReport(delta, method, params)
    worker.postMessage(await tank[method](...params))
  }
  worker.onmessage = (msg) => handleMessage(msg).catch(console.log)
}
