/* global self, main */
/* eslint-disable no-restricted-globals */

function throttle(fn, ms) {
  let stamp = 0
  return (...params) => {
    const now = Date.now()
    if (now - stamp > ms) {
      stamp = now
      fn(...params)
    }
  }
}

(function(_log, _error) {

  console = { log: throttle(_log, 100) }
  self.log = console.log

  function pullResponse() {
    return new Promise((resolve) => {
      self.onmessage = (e) => {
        self.onmessage = undefined
        resolve(e.data)
      }
    })
  }

  const proxyMethod = (method) => (...params) => {
    self.postMessage({ method, params })
    return pullResponse()
  }

  const proxyHandlerMethods = [
    'getPosition', 'getX', 'getY', 'getSpeed', 'getDamage',
    'drive', 'shoot', 'scan'
  ]
  const proxyHandler = proxyHandlerMethods.reduce(
    (proxy, methodName) => Object.assign({}, proxy, {
      [methodName]: proxyMethod(methodName)
    }),
    {}
  )

  main(proxyHandler)
    .then(() => _error('* Handler finished'))
    .catch(e => _error('* Handler error:', e))

}(console.log, console.error))
