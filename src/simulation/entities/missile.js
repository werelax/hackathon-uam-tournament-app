/* global Phaser */

import { config } from '../config'

export class Missile {
  constructor(scene, x, y, direction, range, tank) {
    // simulation
    this.range = range
    this.start = { x, y }
    this.tank = tank
    // engine
    this.scene = scene
    this.matter = this.scene.matter
    this.sprite = this.matter.add.sprite(x, y, 'missile')
    this.sprite.setTint(this.tank.color)
    this.sprite.setFriction(...config.missile.friction)
    this.sprite.setMass(config.missile.mass)
    this.sprite.setRotation(direction)
    this.sprite.thrust(
      config.missile.initialImpulse * config.missile.thrust
    )
    // fire
    this.startEngineFire()
  }

  update() {
    this.sprite.thrust(config.missile.thrust)
    const center = this.sprite.getCenter()
    const distance = Phaser.Math.Distance.Between(
      center.x, center.y, this.start.x, this.start.y
    )
    if (distance > this.range) {
      this.scene.explodeMissile(this)
    }
  }

  addDestroyCallback(fn) {
    this.destroyCallback = fn
  }

  destroy() {
    if (this.destroyCallback) this.destroyCallback()
    this.sprite.destroy()
    this.emitter.destroy()
  }

  is(body) {
    return this.sprite === body.gameObject
  }

  isFrom(tank) {
    return tank === this.tank
  }

  // graphics

  startEngineFire() {
    this.emitter = this.scene.add.particles('fire')
    const fire = this.emitter.createEmitter({
      alpha: { start: .7, end: 0 },
      scale: { start: 0.15, end: 0.4 },
      rotate: { min: -180, max: 180 },
      lifespan: { min: 200, max: 300 },
      speed: { min: 0, max: 40 },
      blendMode: 'ADD',
      maxParticles: 50
    })
    fire.startFollow(this.sprite)
  }
}
