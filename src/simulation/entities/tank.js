/* global Phaser */

import 'phaser'
import { pull, sortBy, head, map } from 'lodash'

import { config } from '../config'

const diff = (a, b) => {
  const max = Math.max(a, b)
  const min = Math.min(a, b)
  return Math.abs(max - min)
}

export class Tank {
  constructor(id, hexColor, x, y) {
    this.id = id
    this.hexColor = hexColor
    this.color = Phaser.Display.Color.HexStringToColor(hexColor).color
    this.initPosition = { x, y }
    this.lifePoints = config.tank.lifePoints
    this.throttle = 0
    this.locks = 0
    this.dead = false
    this.workerStats = { lag: 0 }
  }

  update(finished) {
    if (!finished) {
      this.updateMovement()
      this.updateForces()
    }
    this.updateGraphics()
  }

  addDestroyCallback(cb) {
    this.destroyCallback = cb
  }

  destroy() {
    this.explossionParticles(...this.getPosition())
    if (this.destroyCallback) this.destroyCallback()
    this.sprite.destroy()
    this.cannon.destroy()
    this.dead = true
  }

  is(body) {
    return this.sprite === body.gameObject
  }

  serialize() {
    let lastCommand
    try {
      const params = this.workerStats.params.map(p => Math.round(p))
      lastCommand = this.workerStats.method
        ? `${this.workerStats.method}(${params})`
        : '...'
    } catch(e) {
      lastCommand = '...'
    }
    return {
      id: this.id,
      color: this.hexColor,
      colorIdx: config.tank.colors.indexOf(this.hexColor),
      position: this.dead ? [0, 0] : this.getPosition(),
      rotation: this.dead ? 0 : this.sprite.angle,
      cannon: this.cannon._angle,
      lifePoints: this.lifePoints,
      throttle: this.throttle,
      speed: this.dead ? 0 : this.getSpeed(),
      disabled: this.stopped,
      dead: this.dead,
      lag: this.workerStats.lag,
      lastCommand
    }
  }

  // handler api

  getSpeed() {
    return Math.round(this.sprite.body.speed * 20)
  }

  getDamage() {
    return config.tank.lifePoints - this.lifePoints
  }

  getPosition() {
    const { x, y } = this.sprite.getCenter()
    return [x, y].map(Math.round)
  }

  getInvertedPosition() {
    const { x, y } = this.sprite.getCenter()
    const { height } = this.scene.cameras.main
    return [x, height - y].map(Math.round)
  }

  getX() {
    return this.getPosition()[0]
  }

  getInvertedY() {
    return this.getInvertedPosition()[1]
  }

  drive(angle, power) {
    this.direction = Phaser.Math.DegToRad(360 - angle)
    this.throttle = power
    return this.sprite.speed
  }

  shoot(angle, range) {
    const myMissiles = this.scene.getMissilesForTank(this)
    const missileCount = myMissiles.length
    if (missileCount >= config.tank.maxMissiles) return false
    const direction = this.direction = Phaser.Math.DegToRad(360 - angle)
    const center = this.sprite.getCenter()
    const delta = new Phaser.Math.Vector2(
      config.tank.missileDeadzoneX * Math.cos(direction),
      config.tank.missileDeadzoneY * Math.sin(direction)
    )
    const position = center.add(delta)
    this.scene.spawnMissile(position.x, position.y, direction, range, this)
    this.cannon.rotation = direction
    this.cannon._angle = 360 - angle
    return missileCount + 1
  }

  scan(userAngle, resolution) {
    const angle = 360 - userAngle
    const graph = this.drawScanCone(angle, resolution)
    this.disableTank(
      config.tank.scanDelay,
      () => { graph.destroy() }
    )
    const [x, y] = this.getPosition()
    const from = Phaser.Math.DegToRad(angle - resolution)
    const to = Phaser.Math.DegToRad(angle + resolution)
    const tanks = this.scene.getTanksInSlice(this, x, y, from, to)
    tanks.forEach(tank => tank.shineScan())
    const squared = Phaser.Math.Distance.Squared
    const closer = head(sortBy(
      map(tanks, t => squared(x, y, ...t.getPosition()))
    ))
    return closer ? Math.round(Math.sqrt(closer)) : 0
  }

  // simulation

  updateMovement() {
    const speed = this.getSpeed()
    if ((config.tank.canScanAndDrive || !this.stopped)
        && speed < this.throttle) {
      this.sprite.thrust(this.throttle / 100)
      if (this.getSpeed() < 55) this.adjustAngle()
    }
  }

  adjustAngle() {
    if (diff(this.sprite.rotation, this.direction) > 0.01) {
      const updated = Phaser.Math.Angle.RotateTo(
        this.sprite.rotation, this.direction, config.tank.turnSpeed
      )
      this.sprite.setRotation(updated)
    }
  }

  disableTank(ms, cb) {
    this.stopped = true
    this.locks++
    // if (this.stopTimer) this.stopTimer.remove()
    this.stopTimer = this.scene.time.addEvent({
      delay: ms,
      callback: () => {
        if (--this.locks === 0) { this.stopped = false }
        if (cb) cb()
      }
    })
  }

  stop() {
    this.sprite.setVelocity(0, 0)
    this.disableTank(config.tank.collisionStuntDelay)
  }

  applyDamage(amount) {
    const realAmount = Math.min(config.tank.maxDamagePerHit, amount)
    this.lifePoints -= realAmount
    if (this.lifePoints < 0) {
      this.destroy()
      return true
    } else {
      this.showDamageTint(amount)
      return false
    }
  }

  getLifePoints() {
    return this.lifePoints
  }

  isDisabled() {
    return this.stopped
  }

  workerReport(lag, method, params) {
    this.workerStats = { lag, method, params }
  }

  // graphics

  setScene(scene) {
    this.scene = scene
    this.matter = this.scene.matter
    this.sprite = this.matter.add.sprite(
      this.initPosition.x,
      this.initPosition.y,
      'tank'
    )
    this.sprite.setTint(this.color)
    this.sprite.setScale(config.tank.scale)
    this.sprite.setBounce(config.tank.bounce)
    this.sprite.setFriction(...config.tank.friction)
    this.sprite.setMass(config.tank.mass)
    this.sprite.setOrigin(0.4, 0.5)
    // cannon
    this.cannon = this.scene.add.image(
      this.initPosition.x,
      this.initPosition.y,
      'cannon'
    )
    this.cannon.setScale(config.tank.scale + 0.03)
    this.cannon.setTint(this.color)
    this.cannon.setOrigin(0.5, 0.5)
    // forces
    this.forces = []
  }

  showDamageTint(amount) {
    this.sprite.setTint(0x880000)
    this.cannon.setTint(0x880000)
    if (this.tintTimer) this.tintTimer.remove()
    this.tintTimer = this.scene.time.addEvent({
      delay: 200,
      callback: () => {
        this.sprite.setTint(this.color)
        this.cannon.setTint(this.color)
      }
    })
  }

  updateGraphics() {
    this.cannon.x = this.sprite.x
    this.cannon.y = this.sprite.y
  }

  explossionParticles(x, y) {
    const particles = this.scene.add.particles('fire')
    const { scale, duration } = config.explosions.tank
    const explosion = particles.createEmitter({
      blendMode: 'ADD',
      alpha: { start: 0.7, end: 0 },
      scale: { start: 2 * scale, end: 0.5 },
      tint: { start: 0xff245e, end: 0xffbbdd },
      speed: { min: 50 * scale, max: 200 * scale },
      rotate: { min: -180, max: 180 },
      lifespan: { min: 500 * duration, max: 800 * duration },
      frequency: 1,
      maxParticles: 200,
      x,
      y
    })
    explosion.explode(200)
    this.scene.time.addEvent({
      delay: 1000,
      callback: () => particles.destroy()
    })
  }

  drawScanCone(angle, resolution) {
    const { x, y } = this.sprite.getCenter()
    const start = Phaser.Math.DegToRad(angle - resolution)
    const end = Phaser.Math.DegToRad(angle + resolution)
    const graph = this.scene.add.graphics()
    graph.setBlendMode('ADD')
    // graph.fillStyle(0x00ff00, 0.1)
    graph.fillGradientStyle(
      0x000000,
      this.color,
      0x000000,
      this.color,
      0.2
    )
    graph.slice(x, y, config.tank.scanCodeRadius, start, end, false)
    graph.fillPath()
    return graph
  }

  shineScan() {
    this.sprite.setTintFill(0xffffff)
    this.cannon.setTintFill(0xffffff)
    if (this.shineTimer) this.shineTimer.remove()
    this.shineTimer = this.scene.time.addEvent({
      delay: 12,
      callback: () => {
        this.sprite.setTint(this.color)
        this.cannon.setTint(this.color)
      }
    })
  }

  // physics

  updateForces() {
    this.forces.forEach(({ force, point }) => {
      this.sprite.applyForceFrom(point, force)
    })
    this.forces.length = 0
  }

  applyForceFrom(x, y, force) {
    const v2position = this.sprite.getCenter()
    const v2explossion = new Phaser.Math.Vector2(x, y)
    const v2direction = v2position.subtract(v2explossion)
    const forceVector = v2direction.normalize().scale(force)
    const forceObj = {
      point: v2position,
      force: forceVector
    }
    this.forces.push(forceObj)
  }
}
