import { Missile } from '../entities/missile'
import { Tank } from '../entities/tank'

const getInstanceOf = Klass => (...params) => {
  return params.find(t => t instanceof Klass)
}

export const collisions = {
  getMissile: getInstanceOf(Missile),
  getTank: getInstanceOf(Tank),
  getWall: (...params) => params.some(t => (!t || t.gameObject === null)),
  missileToWall(a, b) {
    const wall = collisions.getWall(a, b)
    const missile = collisions.getMissile(a, b)
    return Boolean(wall && missile)
  },
  missileToTank(a, b) {
    const tank = collisions.getTank(a, b)
    const missile = collisions.getMissile(a, b)
    return Boolean(tank && missile)
  },
  tankToWall(a, b) {
    const wall = collisions.getWall(a, b)
    const tank = collisions.getTank(a, b)
    return Boolean(wall && tank)
  },
  tankToTank(a, b) {
    return (a instanceof Tank) && (b instanceof Tank)
  }
}
