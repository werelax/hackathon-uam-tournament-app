import { map, values, compact, times } from 'lodash'

// state

const state = {
  game: undefined,
  selectedDrivers: {},
  state: 'STOP'
}

// utils

const Q = (...args) => document.querySelector(...args)

const setAttr = (a, v) => s => Q(s).setAttribute(a, v)
const rmAttr = (a) => s => Q(s).removeAttribute(a)
const enable = rmAttr('disabled')
const disable = setAttr('disabled', 'disabled')

const bindClick = (s, f) => Q(s).addEventListener('click', f)

// commands

function renderDriverOptions(drivers) {
  const options = map(drivers, (value, key) => `
    <option value="${key}">${key}</option>
  `).join('')
  return `<option>---</option> ${options}`
}

function renderDriverSelector(key, drivers) {
  const container = Q('#available-drivers')
  const select = document.createElement('select')
  select.innerHTML = renderDriverOptions(drivers)
  select.addEventListener('change', (event) => {
    const { target: { value } } = event
    const driver = { id: value, script: drivers[value] }
    state.selectedDrivers[key] = value ? driver : null
  })
  container.appendChild(select)
}

function renderAvailableDrivers(drivers) {
  times(6, i => renderDriverSelector(i, drivers))
}

const start = startGame => () => {
  disable('#start-button')
  enable('#pause-button')
  enable('#restart-button')
  const drivers = compact(values(state.selectedDrivers))
  state.game = startGame(drivers)
  window.game = state.game
  state.game.events.on('end-game', (score) => {
    console.table(score.calculateScore())
  })
}

function pause() {
  enable('#resume-button')
  disable('#pause-button')
  state.game.scene.pause('MainScene')
}

function resume() {
  enable('#pause-button')
  disable('#resume-button')
  state.game.scene.resume('MainScene')
}

const restart = restartGame => () => {
  const drivers = compact(values(state.selectedDrivers))
  restartGame(state.game, drivers)
}

export function simpleRuntimeUI (drivers, startGame, restartGame) {
  renderAvailableDrivers(drivers)
  bindClick('#start-button', start(startGame))
  bindClick('#pause-button', pause)
  bindClick('#resume-button', resume)
  bindClick('#restart-button', restart(restartGame))

  Q('select:nth-child(1)').value = 'grunt'
  Q('select:nth-child(1)').dispatchEvent(new Event('change'))
  Q('select:nth-child(2)').value = 'grunt'
  Q('select:nth-child(2)').dispatchEvent(new Event('change'))
  Q('select:nth-child(3)').value = 'grunt'
  Q('select:nth-child(3)').dispatchEvent(new Event('change'))
  Q('select:nth-child(4)').value = 'grunt'
  Q('select:nth-child(4)').dispatchEvent(new Event('change'))
}
