/* eslint-disable import/no-webpack-loader-syntax */

import { set, get, mapValues, map, compact } from 'lodash'
import { simpleRuntimeUI } from './ui/simple-runtime-ui'

import { createGame } from './game'
import { MainScene } from './scenes/main'

import { workerWrapper } from './workers/wrapper'


// load all files in /drivers/* as strings
const req = require.context('!raw-loader!./drivers/', true, /\.js$/)
const drivers = req.keys().reduce((acc, file) => {
  const name = file.replace('.js', '').replace('./', '')
  return set(acc, name, req(file))
}, {})

export const wrappedDrivers = mapValues(drivers, d => workerWrapper(d))

export function startGame(selectedDrivers) {
  const drivers = compact(map(selectedDrivers, ({ id, color }) => {
    if (!id) return null
    return { id, color, script: get(wrappedDrivers, id) }
  }))
  const mainScene = new MainScene(drivers)
  const game = createGame(mainScene)
  return game
}

export function pauseGame(game, isPaused) {
  if (isPaused) game.scene.resume('MainScene')
  else game.scene.pause('MainScene')
}

export function endGame(game) {
  const mainScene = game.scene.getScene('MainScene')
  mainScene.endGame()
}

export function destroyGame(game) {
  game.scene.remove('MainScene')
  game.destroy(true)
}

export function restartGame(game, drivers) {
  game.scene.remove('MainScene')
  game.scene.add('MainScene', new MainScene(drivers), true)
}
