import { assign } from 'lodash'
import { TankHandler } from './tank-handler'

export class Simulation {
  constructor(game) {
    this.game = game
    this.tankHandlers = []
    assign(game, {
      reportWorldState: this.onReportWorldState.bind(this),
      startSimulation: this.startSimulation.bind(this),
      updateSimulation: this.updateSimulation.bind(this),
      tankHandlers: this.tankHandlers
    })
  }
  initTankDrivers() {
    for (const Driver of window.TANK_DRIVERS) {
      const driver = new Driver()
      this.tankHandlers.push(driver)
    }
  }
  startSimulation() {
    this.initTankDrivers()
    this.game.scene.start('MainScene')
  }
  updateSimulation() {
    for (const tankHandler of this.tankHandlers) {
      tankHandler.executeCommand()
    }
  }
  onReportWorldState(tanks, missiles) {
    const tanksUl = document.querySelector('#tanks')
    tanksUl.innerHTML = tanks.map((tank, i) => `2
<li>Tank ${i}: ${tank.getLifePoints()}, ${tank.getPosition()} </li>
`).join('')
  }
}
