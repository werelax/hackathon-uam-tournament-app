import { last, isEmpty } from 'lodash'

export class ScoreManager {
  constructor() {
    this.hits = []
    this.deads = []
    this.tanks = []
  }
  registerDamage(source, target, amount, isKill) {
    const selfInflicted = source === target
    this.hits.push({
      source, target, amount, isKill, selfInflicted
    })
  }
  registerDead(time, killer, victim) {
    const selfInflicted = killer === victim
    this.deads.push({ time, killer, victim, selfInflicted })
    this.tanks.push(victim)
  }
  registerWinners(winners) {
    this.winners = winners
    winners.forEach(w => this.tanks.push(w))
  }
  calculateScore() {
    const score = {}
    this.winners.forEach((winner) => {
      score[winner.id] = this.getScorePerSurvivor()
    })
    if (!isEmpty(this.deads)) {
      const second = last(this.deads).victim
      score[second.id] = this.getScoreForSecond()
    }
    return this.tanks
      .map((tank) => ({
        id: tank.id,
        score: score[tank.id] || 0,
        kills: this.getVictims(tank),
        killedBy: this.getKiller(tank),
        inflictedDamage: this.getInflictedDamage(tank)
      }))
      .sort((a, b) => b.score - a.score)
  }
  // private methods
  getScorePerSurvivor() {
    switch (this.winners.length) {
      case 1: { return 7 }
      case 2: { return 4 }
      case 3: { return 2 }
      default: { return 1 }
    }
  }
  getScoreForSecond() {
    switch (this.winners.length) {
      case 1: { return 3 }
      case 2: { return 1 }
      default: { return 0 }
    }
  }
  getVictims(tank) {
    return this.deads
      .filter(({ killer }) => killer === tank)
      .map(({ victim }) => victim.id)
  }
  getKiller(tank) {
    return this.deads
      .filter(({ victim }) => victim === tank)
      .map(({ killer }) => killer.id)[0]
  }
  getInflictedDamage(tank) {
    return this.hits.reduce((acc, { source, amount }) => {
      return (source === tank) ? acc + amount : acc
    }, 0)
  }
//   const selfInflicted = source === target
//   this.hits.push({
//     source, target, amount, isKill, selfInflicted
//   })
// }
// registerDead(time, killer, victim) {
//   const selfInflicted = killer === victim
//   this.deads.push({ time, killer, victim, selfInflicted })
//   this.tanks.push(victim)
// }
}
