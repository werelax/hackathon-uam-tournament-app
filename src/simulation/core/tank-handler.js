import { config } from '../config'

const clamp = (v, min, max) => Math.min(max, Math.max(min, v))

export class TankHandler {
  constructor(driver, tank) {
    this.tank = tank
    this.driver = driver
    // driver(this).then(
    //   () => console.error('* tank driver ended.'),
    //   (e) => e && console.error(e)
    // )
    driver(this)
  }

  removeTank() {
    this.tank = undefined
    if (this.cancelCommand) this.cancelCommand()
  }

  workerReport(...args) {
    this.tank && this.tank.workerReport(...args)
  }

  async enqueueCommand(thunk) {
    if (this.future) {
      throw new Error('* Tank command in progress!')
    }
    this.future = new Promise((resolve, reject) => {
      this.executeCommand = () => {
        const result = thunk()
        resolve(result)
        this.future = false
        this.executeCommand = false
        this.cancelCommand = false
      }
      this.cancelCommand = reject
    })
    return this.future
  }

  update() {
    if (this.tank && !this.tank.isDisabled() && this.executeCommand) {
      this.executeCommand()
    }
  }

  // public interface: queries

  getPosition() {
    return this.enqueueCommand(
      () => this.tank.getInvertedPosition()
    )
  }

  getX() {
    return this.enqueueCommand(() => this.tank.getX())
  }

  getY() {
    return this.enqueueCommand(() => this.tank.getInvertedY())
  }

  getSpeed() {
    return this.enqueueCommand(() => this.tank.getSpeed())
  }

  getDamage() {
    return this.enqueueCommand(() => this.tank.getDamage())
  }

  // public interface: commands

  drive(angle, speed) {
    return this.enqueueCommand(
      () => this.tank.drive(
        clamp(angle % 360, 0, 359), clamp(speed, 0, 100)
      )
    )
  }

  shoot(angle, range) {
    return this.enqueueCommand(
      () => this.tank.shoot(
        clamp(angle % 360, 0, 359),
        clamp(range, 0, config.missile.maxRange)
      )
    )
  }

  scan(angle, resolution) {
    return this.enqueueCommand(
      () => this.tank.scan(
        clamp(angle % 360, 0, 359),
        clamp(resolution, 0, config.tank.maxScanAmplitude)
      )
    )
  }
}
