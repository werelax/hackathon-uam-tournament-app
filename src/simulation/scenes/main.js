/* global Phaser */

import 'phaser'
import { pull, shuffle } from 'lodash'
import { Missile } from '../entities/missile'
import { Tank } from '../entities/tank'
import { TankHandler } from '../core/tank-handler'
import { ScoreManager } from '../core/score-manager'
import { collisions } from '../utils/collisions'

import { config } from '../config'

const Between = Phaser.Math.Between
const PI_2 = 2 * Math.PI

const rand = (n) => Math.floor(Math.random() * n)
const prob = n => Math.random() < n
const between = (from, to, v) => ((v >= from) && (v <= to))

export class MainScene extends Phaser.Scene {
  constructor(drivers) {
    super('MainScene')
    this.drivers = drivers
    this.score = new ScoreManager()
    this.onCollision = this.onCollision.bind(this)
  }

  // system

  create() {
    this.add.image(
      config.arena.width / 2,
      config.arena.height / 2,
      'background'
    )
    this.matter.world.setBounds()
    this.matter.world.disableGravity()
    this.matter.world.on('collisionstart', this.onCollision)
    this.tanks = []
    this.deadTanks = []
    this.missiles = []
    this.tankHandlers = []
    this.explosions = []
    this.populateTanks()
    this.game.events.emit('game-ready')
  }

  populateTanks() {
    const separation = PI_2 / this.drivers.length
    const baseX = this.cameras.main.centerX - 50
    const baseY = this.cameras.main.centerY
    const radius = this.cameras.main.height / 3
    const positions = shuffle(this.drivers.map((_, i) => ([
      baseX + radius * Math.cos(i * separation),
      baseY + radius * Math.sin(i * separation)
    ])))
    this.drivers.forEach(({ id, color, script }, i) => {
      const tank = new Tank(
        id,
        color,
        ...positions[i]
      )
      const tankHandler = new TankHandler(script, tank)
      tank.setScene(this)
      tank.addDestroyCallback(() => {
        tankHandler.removeTank(tank)
        pull(this.tanks, tank)
        pull(this.tankHandlers, tankHandler)
        this.deadTanks.push(tank)
      })
      this.tanks.push(tank)
      this.tankHandlers.push(tankHandler)
    })
  }

  update() {
    if (this.finished) {
      for (const t of this.tanks) t.update(true)
    } else {
      for (const th of this.tankHandlers) th.update()
      for (const m of this.missiles) m.update()
      for (const t of this.tanks) t.update()
      this.checkEndgameConditions()
      this.game.events.emit('update-world-state', {
        tanks: [...this.tanks, ...this.deadTanks].map(t => t.serialize())
      })
    }
  }

  // simulation

  checkEndgameConditions() {
    if (!this.finished && this.tanks.length <= 1 || false) {
      this.endGame()
    }
  }

  endGame() {
    setTimeout(() => this.setTimeScale(0.1), 200)
    this.score.registerWinners(this.tanks)
    this.game.events.emit('end-game', this.score.calculateScore())
    this.finished = true
  }

  onCollision({ pairs }) {
    pairs.forEach(({ bodyA, bodyB }) => {
      const objects = [...this.missiles, ...this.tanks]
      const a = objects.find(obj => obj.is(bodyA))
      const b = objects.find(obj => obj.is(bodyB))
      switch (true) {
        case collisions.missileToWall(a, b):
        case collisions.missileToTank(a, b): {
          const missile = collisions.getMissile(a, b)
          const tank = collisions.getTank(a, b)
          this.explodeMissile(missile, !!tank)
          break
        }
        case collisions.tankToTank(a, b): {
          a.stop()
          b.stop()
          if (this.finished) return
          this.damageTank(a, b, config.damage.tankCollision)
          this.damageTank(b, a, config.damage.tankCollision)
          break
        }
        case collisions.tankToWall(a, b): {
          const tank = collisions.getTank(a, b)
          tank.stop()
          if (this.finished) return
          this.damageTank(tank, tank, config.damage.wallCollision)
          break
        }
      }
    })
  }

  damageTank(target, source, amount) {
    const now = this.time.now
    const isKill = target.applyDamage(amount)
    this.score.registerDamage(source, target, amount, isKill)
    if (isKill) this.score.registerDead(now, source, target)
  }

  calculateDamage(distance, isDirectHit) {
    const explosionDamage = config.damage.missileBaseDamage / distance
    const hitDamage = config.damage.missileHitDamage
    return Math.floor(explosionDamage + (isDirectHit ? hitDamage : 0))
  }

  getMissilesForTank(tank) {
    return this.missiles.filter(m => m.isFrom(tank))
  }

  spawnMissile(x, y, direction, range, tank) {
    const missile = new Missile(this, x, y, direction, range, tank)
    missile.addDestroyCallback(() => pull(this.missiles, missile))
    this.missiles.push(missile)
  }

  explodeMissile(missile, isDirectHit) {
    const { x, y } = missile.sprite
    this.explosion(x, y, isDirectHit, missile.tank)
    missile.destroy()
  }

  getTanksInSlice(tankScanning, x, y, from, to) {
    return this.tanks.filter((tank) => {
      if (tank === tankScanning) return false
      let angle = Phaser.Math.Angle.Normalize(
        Phaser.Math.Angle.Between(x, y, ...tank.getPosition())
      )
      if (between(from, to, angle)) {
        return true
      } else if (from < 0) {
        return  between(from, to, angle - PI_2)
      } else if (to >= PI_2) {
        return between(from, to, angle + PI_2)
      } else {
        return false
      }
    })
  }

  // graphics

  setTimeScale(timeScale) {
    this.time.timeScale = timeScale
    this.matter.world.engine.timing.timeScale = timeScale / 2
    this.explosions.forEach(e => e.timeScale = timeScale)
  }

  explosionParticles(x, y) {
    const { scale, duration } = config.explosions.missile
    const particles = this.add.particles('fire')
    const explosion = particles.createEmitter({
      blendMode: 'ADD',
      alpha: { start: 1, end: 0 },
      scale: { start: 0.3 * scale, end: 2.5 * scale },
      tint: { start: 0xff945e, end: 0xff945e },
      speed: { min: 50 * scale, max: 200 * scale },
      rotate: { min: -180, max: 180 },
      lifespan: { min: 500 * duration, max: 800 * duration },
      frequency: 1,
      maxParticles: 30,
      x,
      y
    })
    explosion.explode(20)
    this.explosions.push(particles)
    this.time.addEvent({
      delay: 1000 * duration,
      callback: () => {
        particles.destroy()
        pull(this.explossions, particles)
      }
    })
  }

  // physics

  explosionForce(x, y, isDirectHit, sourceTank) {
    this.tanks.forEach((tank) => {
      const distance = Phaser.Math.Distance.Between(
        x, y, ...tank.getPosition()
      )
      if (distance < config.damage.explosionRadius) {
        const damage = this.calculateDamage(distance, isDirectHit)
        const force = config.explosions.force / distance
        const finalForce = Math.min(force, config.explosions.maxForce)
        tank.applyForceFrom(x, y, finalForce)
        this.damageTank(tank, sourceTank, damage)
      }
    })
  }

  explosion(x, y, isDirectHit, sourceTank) {
    this.explosionParticles(x, y)
    if (this.finished) return
    this.explosionForce(x, y, isDirectHit, sourceTank)
  }

}
