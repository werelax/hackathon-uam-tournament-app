/* global Phaser */

import 'phaser'
import { assign } from 'lodash'

import { config } from './config'

class BootGame extends Phaser.Scene {
  constructor() {
    super('BootGame')
  }
  preload() {
    this.load.image('tank', '/assets/tank-body.png')
    this.load.image('cannon', '/assets/cannon.png')
    this.load.image('missile', '/assets/bullet.png')
    this.load.image('fire', '/assets/muzzleflash3.png')
    this.load.image('background', '/assets/arena.png')
  }
  create() {
    this.game.scene.start('MainScene')
  }
}

const GAME_CONFIG = {
  type: Phaser.AUTO,
  parent: 'simulation-arena',
  width: config.arena.width,
  height: config.arena.height,
  // transparent: true,
  pixelArt: true,
  physics: {
    default: 'matter',
    matter: {
      debug: false
    }
  }
}

export function createGame(...scenes) {
  return new Phaser.Game(assign({}, GAME_CONFIG, {
    scene: [BootGame, ...scenes]
  }))
}
