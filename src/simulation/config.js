export const config = {
  arena: {
    width: 1420 - 80,
    height: 1080 - 80
  },
  damage: {
    tankCollision: 5,
    wallCollision: 5,
    missileBaseDamage: 400,
    missileHitDamage: 15,
    // explosionRadius: 200
    explosionRadius: 100
  },
  explosions: {
    force: 400,
    maxForce: 200,
    // tank: { scale: 1, duration: 1 },
    tank: { scale: 0.8, duration: 1 },
    // missile: { scale: 1, duration: 1 }
    missile: { scale: 0.5, duration: 0.7 }
  },
  tank: {
    colors: [
      '#77FF38',
      '#FAD811',
      '#C820D2',
      '#3DBCCD',
      '#6A0AF8',
      '#FF8426'
    ],
    canScanAndDrive: true,
    maxScanAmplitude: 10,
    maxDamagePerHit: 25,
    // scale: 0.3,
    scale: 0.33,
    bounce: 0,
    friction: [.07, .07, .07],
    mass: 1000,
    scanDelay: 200,
    lifePoints: 100,
    maxMissiles: 2,
    turnSpeed: 0.3,
    collisionStuntDelay: 200,
    missileDeadzoneX: 70,
    missileDeadzoneY: 70,
    scanCodeRadius: 1200
  },
  missile: {
    maxRange: 700,
    //  maxRange: 1200,
    mass: 100,
    thrust: 0.01,
    initialImpulse: 220,
    friction: [0, 0, 0]
  }
}
